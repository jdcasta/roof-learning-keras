""" This module contains utility functions """
import os
import glob
import math
from math import trunc
import itertools
import json
import functools
from functools import partial

import numpy as np
import matplotlib

import keras
import keras.preprocessing.image


matplotlib.use('Agg')  # fixes issue if no GUI provided
import matplotlib.pyplot as plt

import seaborn as sns
# sns.set(style='white')

import numpy as np
import pandas as pd
import importlib
from keras import backend as K

from keras.preprocessing.image import ImageDataGenerator
from keras.applications.vgg16 import preprocess_input as preprocess_input_vgg16

from contrib_preprocessing import monkey_patch_all
monkey_patch_all()

import config


def dictproduct(dct):
    for t in itertools.product(*dct.values()):
        yield dict(zip(dct.keys(), t))


def read_test_file(filename):
    with open(filename) as data_file:
        data = json.load(data_file)
    test_sets = []
    for test_set in data['test_sets']:
        defaults = dict(data['defaults'])
        defaults.update(test_set)
        if test_set.get('active', True):
            test_sets.append(defaults)

    return test_sets, data.get('name', 'UK')


def actual_kwargs():
    """
    Decorator that provides the wrapped function with an attribute 'actual_kwargs'
    containing just those keyword arguments actually passed in to the function.
    """
    def decorator(function):
        def inner(*args, **kwargs):
            inner.actual_kwargs = kwargs
            return function(*args, **kwargs)
        return inner
    return decorator


class Iterator(object):
    def __init__(self, generator, original):
        # This is the mapped generator, dual outpouts, preprocesing done
        self.generator = generator
        # The original, underlying generator
        if hasattr(original, 'original'):  # dual generator
            self.original = original.original
        else:
            self.original = original
        self.class_indices = original.class_indices

    def __iter__(self):
        # Needed if we want to do something like:
        # for x, y in data_gen.flow(...):
        return self.generator

    def __next__(self, *args, **kwargs):
        return self.generator.__next__(*args, **kwargs)


def rgb_to_bgr(gen, data_format='channels_last'):
    """
    This takes a generator providing batches of images, and convert
    them from RGB to BGR Channel Ordering.  This is needed for many
    imagenet models
    """
    # data_format = K.image_data_format()
    batch_x, batch_y = gen[0], gen[1]
    if data_format == 'channels_first':
        batch_x = batch_x[:, ::-1, :, :]
    else:
        batch_x = batch_x[:, :, :, ::-1]
    return batch_x, batch_y


def preprocess_image_net(gen, preprocess_input=preprocess_input_vgg16):
    """
    This preprocess function will perform mean centering and change RGB -> BGR
    This function works for the following Models VGG16, VGG19, RESNET, XCEPTION
    """
    batch_x, batch_y = gen[0], gen[1]
    batch_x = preprocess_input(batch_x)
    return batch_x, batch_y


def format_gen_outputs(gen1, gen2):
    """ Maps two generators into one! """
    x1 = gen1[0]
    x2 = gen2[0]
    y1 = gen1[1]
    return [x1, x2], y1


def flow_generator_df(dataframe, directory, data=None, modality='rgb', target_size=(224, 224), batch_size=32, seed=1, augment=True, shuffle=True, preprocess_input=preprocess_input_vgg16):
    """
    Creates a generator thats pulls images out of a dataframe
    The data is also augmented. Data is mean centered/normalized IF a sample set is provided
    """
    # pull the training/validation/testing subset from the data frame
    df = dataframe

    # Dictionaries for arguments to passed to ImageDataGenerator
    arguments = {}  # default arguments
    augmentation = {"horizontal_flip": True, "vertical_flip": True, 'rotation_range': 45,
                    'fill_mode': 'constant', 'cval': 255, 'zoom_range': [1, 1.15]}
    mean = {"featurewise_center": True, "featurewise_std_normalization": True}

    # If we want to augment the data
    if augment:
        arguments.update(augmentation)
    # If we have provided sample data to mean normalize with
    if data is not None:
        arguments.update(mean)
        img_gen = ImageDataGenerator(**arguments)
        img_gen.fit(data, augment=True, seed=seed)
    else:
        img_gen = ImageDataGenerator(**arguments)

    data_generator = img_gen.flow_from_dataframe(
        dataframe=df,
        directory=directory,
        class_column='class_' + modality,
        src_column='src_' + modality,
        target_size=target_size,
        class_mode='categorical',
        classes=config.classes,
        #     save_to_dir=rgb_save,
        seed=seed,
        batch_size=batch_size,
        shuffle=shuffle)

    """
    Modify the Data Generator to do some last minute preprocessing
    Iterator allows us to save the original ImageDataGeneratorObject after the map is done
    You can save attributes to Class Objects, but not to map. ImageDataGenerator has lots of 
    attributes that would have been lost. class_indices, file_names, etc.
    """
    if data is not None:
        print('Modify BGR')
        # still need to switch from RGB -> BGR Channels, no mean centering though
        data_generator_new = Iterator(
            map(rgb_to_bgr, data_generator), data_generator)
    else:
        # Applys the Preprocessing Function
        preprocess_function = partial(
            preprocess_image_net, preprocess_input=preprocess_input)
        data_generator_new = Iterator(
            map(preprocess_function, data_generator), data_generator)

    data_generator_new.class_indices = data_generator.class_indices

    return data_generator_new


def flow_generator(directory, data=None, target_size=(224, 224), batch_size=32, seed=1, augment=True, shuffle=True, preprocess_input=preprocess_input_vgg16):
    """
    Creates a generator thats pulls images out of class hierarchical folder structure
    The data is also augmented. Data is mean centered/normalized IF a sample set is provided
    """
    # Dictionaries for arguments to passed to ImageDataGenerator
    arguments = {}  # default arguments
    augmentation = {"horizontal_flip": True, "vertical_flip": True, 'rotation_range': 45,
                    'fill_mode': 'constant', 'cval': 255, 'zoom_range': [1, 1.15]}
    mean = {"featurewise_center": True, "featurewise_std_normalization": True}

    # If we want to augment the data
    if augment:
        arguments.update(augmentation)
    # If we have provided sample data to mean normalize with
    if data is not None:
        arguments.update(mean)
        img_gen = ImageDataGenerator(**arguments)
        img_gen.fit(data, augment=True, seed=seed)
    else:
        img_gen = ImageDataGenerator(**arguments)

    # img_gen.random_transform = functools.partial(monkeypatch.random_transform, img_gen)
    data_generator = img_gen.flow_from_directory(
        directory,
        class_mode='categorical',
        #     save_to_dir=rgb_save,
        seed=seed,
        target_size=target_size,
        batch_size=batch_size,
        shuffle=shuffle)

    """
    Modify the Data Generator to do some last minute preprocessing
    Iterator allows us to save the original ImageDataGeneratorObject after the map is done
    You can save attributes to Class Objects, but not to map. ImageDataGenerator has lots of 
    attributes that would have been lost. class_indices, file_names, etc.
    """
    if data is not None:
        print('Modify BGR')
        # still need to switch from RGB -> BGR Channels, no mean centering though
        data_generator_new = Iterator(
            map(rgb_to_bgr, data_generator), data_generator)
    else:
        # Applys the Preprocessing Function
        preprocess_function = partial(
            preprocess_image_net, preprocess_input=preprocess_input)
        data_generator_new = Iterator(
            map(preprocess_function, data_generator), data_generator)

    data_generator_new.class_indices = data_generator.class_indices

    return data_generator_new


def get_samples(directory, samples=1000, target_size=(224, 224)):
    """
    Returns a subset of the images and their classifications from a directory
    Use this if you have A LOT of images that cant fit into RAM, and you need a sample set to compute statistics
    for mean centering and normalization
    """
    datagen_simple = ImageDataGenerator()

    generator = datagen_simple.flow_from_directory(
        directory,
        class_mode='categorical',
        target_size=target_size,
        seed=1,
        batch_size=samples)
    x, y = generator.next()
    return x, y


def image_generator(directory, target_size=(224, 224), batch_size=32, seed=1, sample_mean=False, augment=True, shuffle=True, preprocess_input=preprocess_input_vgg16):
    """
    Creates an image generator given a target directory. The images generated will be
    augmented.  If sample_mean is set to true, then instead of subtracting the mean using
    ImageNets means, we will sample from the directory picture population and perform mean centering
    and normalization on it.
    """
    x_data = None
    if sample_mean:
        x_data, _ = get_samples(directory, samples=1000,
                                target_size=target_size)
    return flow_generator(directory, data=x_data, target_size=target_size, batch_size=batch_size, seed=seed, augment=augment, shuffle=shuffle, preprocess_input=preprocess_input)


def image_generator_df(dataframe, directory, modality='rgb', target_size=(224, 224), batch_size=32, seed=1, sample_mean=False, augment=True, shuffle=True, preprocess_input=preprocess_input_vgg16):
    x_data = None
    if sample_mean:
        # TODO - Fix this to work with dataframe
        x_data, _ = get_samples(directory, samples=1000,
                                target_size=target_size)

    if modality == 'dual':
        print("Creating Dual image Generator")
        rgb_gen = flow_generator_df(dataframe, directory, data=x_data, modality='rgb', target_size=target_size,
                                    batch_size=batch_size, seed=seed, augment=augment, shuffle=shuffle, preprocess_input=preprocess_input)
        lidar_gen = flow_generator_df(dataframe, directory, data=x_data, modality='lidar', target_size=target_size,
                                      batch_size=batch_size, seed=seed, augment=augment, shuffle=shuffle, preprocess_input=preprocess_input)
        combo_gen = Iterator(
            map(format_gen_outputs, rgb_gen, lidar_gen), rgb_gen)
        return combo_gen
    else:
        return flow_generator_df(dataframe, directory, data=x_data, modality=modality, target_size=target_size, batch_size=batch_size, seed=seed, augment=augment, shuffle=shuffle, preprocess_input=preprocess_input)


def dual_image_generator(rgb_dir, lidar_dir, target_size=(224, 224), batch_size=32, seed=1, sample_mean=False, augment=True, shuffle=True, preprocess_input=preprocess_input_vgg16):
    """
    This creates a single generator which is constructed by creating two generators,
    one for rgb (aerial) and one for lidar.  The generators are then combined together.
    """

    x_rgb = None
    x_lidar = None

    if sample_mean:
        x_rgb, _ = get_samples(rgb_dir, samples=1000, target_size=target_size)
        x_lidar, _ = get_samples(
            lidar_dir, samples=1000, target_size=target_size)

    rgb_gen = flow_generator(
        rgb_dir, data=x_rgb, target_size=target_size, batch_size=batch_size, seed=seed, augment=augment, shuffle=shuffle, preprocess_input=preprocess_input)

    lidar_gen = flow_generator(
        lidar_dir, data=x_lidar, target_size=target_size, batch_size=batch_size, seed=seed, augment=augment, shuffle=shuffle, preprocess_input=preprocess_input)

    combo_gen = Iterator(map(format_gen_outputs, rgb_gen, lidar_gen), rgb_gen)

    return combo_gen


# def save_history(history, prefix):
#     if 'acc' not in history.history:
#         return

#     if not os.path.exists(config.plots_dir):
#         os.mkdir(config.plots_dir)

#     img_path = os.path.join(config.plots_dir, '{}-%s.jpg'.format(prefix))

#     # summarize history for accuracy
#     plt.plot(history.history['acc'])
#     plt.plot(history.history['val_acc'])
#     plt.title('model accuracy')
#     plt.ylabel('accuracy')
#     plt.xlabel('epoch')
#     plt.legend(['train', 'test'], loc='upper left')
#     plt.savefig(img_path % 'accuracy')
#     plt.close()

#     # summarize history for loss
#     plt.plot(history.history['loss'])
#     plt.plot(history.history['val_loss'])
#     plt.title('model loss')
#     plt.ylabel('loss')
#     plt.xlabel('epoch')
#     plt.legend(['train', 'test'], loc='upper right')
#     plt.savefig(img_path % 'loss')
#     plt.close()


def plot_confusion_matrix(cm, classes,
                          accuracy,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues,
                          sub_dir=None,
                          output='',
                          dataset='UK'):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    with sns.axes_style("white"):
        confusion_matrix_dir = './confusion_matrix_plots'
        if not os.path.exists(confusion_matrix_dir):
            os.mkdir(confusion_matrix_dir)

        if normalize:
            cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
            print("Normalized confusion matrix")
        else:
            print('Confusion matrix, without normalization')

        plt.cla()
        plt.figure()
        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        if accuracy is not None:
            acc_title = title + \
                ". {:d}% accuracy".format(trunc(accuracy * 100))
            plt.title(acc_title)
        plt.colorbar()
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=45)
        plt.yticks(tick_marks, classes)

        print(cm)
        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 1.25
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            # print(cm[i,j], thresh)
            color = "#BFD1D4" if cm[i, j] > thresh else "black"
            plt.text(j, i, format(cm[i, j], fmt),
                     horizontalalignment="center",
                     color=("#BFD1D4" if cm[i, j] > thresh else "black"))

        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')

        end_suffix = sub_dir if sub_dir else 'dual'

        if normalize:
            plt.savefig(os.path.join(confusion_matrix_dir,
                                     '{}_{}_ normalized_{}.png'.format(output, dataset, end_suffix)))
        else:
            plt.savefig(os.path.join(confusion_matrix_dir,
                                     '{}_{}_without_normalization_{}.png'.format(output, dataset, end_suffix)))
        plt.cla()
        plt.clf()


def get_subset_df(df, train_val_test='train'):
    return df[df['train_val_test'] == train_val_test]


def change_class_df(df, from_class, to_class):
    mask1 = df['class_lidar'].isin(from_class)
    mask2 = df['class_rgb'].isin(from_class)
    df.loc[mask1, 'class_lidar'] = to_class
    df.loc[mask2, 'class_rgb'] = to_class

def set_samples_info(class_modifier='none'):
    """
    Go through dataframe and determine the number of samples
    """
    # print(config.classes)
    # config.df = config.df[config.df.isin(config.classes)]
    modality = config.data_sub_dir if config.data_sub_dir != 'dual' else 'rgb'

    # Remove Classes that dont belong
    config.df = config.df[config.df['class_' + modality].isin(config.classes)]

    # Modify classes
    # class_modifier = ['none', 'collapseflat', 'flatvsall']
    # collapseflat - collapses the complex_flat and flat class into one
    # flatvsall - flat vs non flat
    if class_modifier == 'collapseflat' or class_modifier == 'flatvsall':
        change_class_df(config.df, ['complex_flat'], 'flat')
        config.classes.remove('complex_flat')
    
    if class_modifier == 'flatvsall':
        temp_class_list = list(config.classes)
        temp_class_list.remove('flat')
        change_class_df(config.df, temp_class_list, 'complex')
        config.classes = ['complex', 'flat']


    config.df_train = get_subset_df(config.df, 'train')
    config.df_val = get_subset_df(config.df, 'val')
    config.df_test = get_subset_df(config.df, 'test')

    config.nb_train_samples = config.df_train.shape[0]
    config.nb_validation_samples = config.df_val.shape[0]
    config.nb_test_samples = config.df_test.shape[0]

    print(config.nb_train_samples,
          config.nb_validation_samples, config.nb_test_samples)

# def set_samples_info():
#     """
#     Walks through the train and valid directories
#     and returns number of images.  This will inform our generators
#     """
#     white_list_formats = {'png', 'jpg', 'jpeg', 'bmp'}

#     train_key = os.path.abspath(config.train_dirs[0])
#     validation_key = os.path.abspath(
#         config.validation_dirs[0]) if config.validation_dirs else None
#     test_key = os.path.abspath(
#         config.test_dirs[0]) if config.test_dirs else None

#     dirs_info = {train_key: 0, validation_key: 0, test_key: 0}
#     for d in dirs_info:
#         # Skip directory if it doesn't exist
#         if d is None:
#             continue
#         dirs_info[d] = count_images(d)

#     config.nb_train_samples = dirs_info[train_key]
#     config.nb_validation_samples = dirs_info[validation_key]
#     config.nb_test_samples = dirs_info[validation_key]


def count_images(directory):
    " Count images "
    white_list_formats = {'png', 'jpg', 'jpeg', 'bmp'}
    dir_info = 0
    iglob_iter = glob.iglob(directory + '/**/*.*', recursive=True)
    for i in iglob_iter:
        filename, file_extension = os.path.splitext(i)
        if file_extension[1:] in white_list_formats:
            dir_info += 1
    return dir_info


def get_class_weight(df):
    try:
        modality = config.data_sub_dir if config.data_sub_dir != 'dual' else 'rgb'
        class_number = df.groupby(['class_' + modality]).size().to_dict()

        total = np.sum(list(class_number.values()))
        max_samples = np.max(list(class_number.values()))
        mu = 1. / (total / float(max_samples))
        class_names = class_number.keys()
        class_weight = dict()
        mapping = get_classes_in_keras_format()
        for class_name in class_names:
            key = mapping[class_name]
            score = math.log(mu * total / float(class_number[class_name]))
            class_weight[key] = score if score > 1. else 1.
    except Exception as e:
        print("No training data, in csv file....class weight all equal")
        class_weight = np.ones(len(config.classes))

    return class_weight

# def get_class_weight(d):
#     white_list_formats = {'png', 'jpg', 'jpeg', 'bmp'}
#     class_number = dict()
#     dirs = sorted([o for o in os.listdir(
#         d) if os.path.isdir(os.path.join(d, o))])
#     k = 0
#     for class_name in dirs:
#         class_number[k] = 0
#         iglob_iter = glob.iglob(os.path.join(d, class_name, '*.*'))
#         for i in iglob_iter:
#             _, ext = os.path.splitext(i)
#             if ext[1:] in white_list_formats:
#                 class_number[k] += 1
#         k += 1

#     total = np.sum(list(class_number.values()))
#     max_samples = np.max(list(class_number.values()))
#     mu = 1. / (total / float(max_samples))
#     keys = class_number.keys()
#     class_weight = dict()
#     for key in keys:
#         score = math.log(mu * total / float(class_number[key]))
#         class_weight[key] = score if score > 1. else 1.

#     return class_weight


def set_classes_from_train_dir():
    """Returns classes based on directories in train directory"""
    config.classes = sorted(list(config.df['class_rgb'].unique()))


def get_classes_in_keras_format():
    if config.classes:
        return dict(zip(config.classes, range(len(config.classes))))
    return None


def get_model_class_instance(*args, **kwargs):
    """
    Dynamically loads the correct model class
    """
    module = importlib.import_module("models.{}".format(config.model))
    return module.inst_class(*args, **kwargs)


def lock():
    if os.path.exists(config.lock_file):
        exit('Previous process is not yet finished.')
    lock_file = open(config.lock_file, 'w')
    lock_file.write(str(os.getpid()))
    lock_file.close()


def unlock():
    if os.path.exists(config.lock_file):
        os.remove(config.lock_file)
