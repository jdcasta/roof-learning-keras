from keras.applications.inception_v3 import InceptionV3 as KerasInceptionV3
from keras.layers import GlobalAveragePooling2D, Dense, Flatten, Dropout, Lambda
from keras.models import Model
from keras.optimizers import SGD
from keras.preprocessing import image
import numpy as np

import config
from .base_model import BaseModel


class InceptionV3(BaseModel):

    def __init__(self, *args, **kwargs):
        super(InceptionV3, self).__init__(*args, **kwargs)

        if not self.freeze_layers_number:
            # we chose to train the top 2 identity blocks and 1 convolution block
            self.freeze_layers_number = 80

        # self.img_size = (299, 299)

    @staticmethod
    def _create_base(input_tensor, base_name, pooling='avg'):
        " Creates the input and output layers (before softmax) for this model "
        base_model = KerasInceptionV3(weights='imagenet', include_top=False, input_tensor=input_tensor, pooling=pooling)
        BaseModel.rename_layers(base_model, base_name)
        BaseModel.make_net_layers_non_trainable(base_model)

        x = base_model.output
        x = Lambda(lambda x: x, name=base_name + '_features')(x)
        # x = Flatten(name=base_name + '_flatten')(x)

        if config.FC1_SIZE != 0:
            x = Dense(config.FC1_SIZE, activation='elu',
                        name=base_name + '_fc1')(x)
            x = Dropout(0.5, name=base_name + '_dropout')(x)

        return (base_model, x)

    def _create(self):
        base_model, x = InceptionV3._create_base(
            self.get_input_tensor(), config.data_sub_dir, pooling=self.pooling)

        predictions = Dense(len(config.classes), activation='softmax',
                            name=config.data_sub_dir + '_predictions')(x)

        self.model = Model(inputs=base_model.input, outputs=predictions)

        return self.model

    @staticmethod
    def preprocess_input( x):
        " Preprocess input for Inception "
        x /= 255.
        x -= 0.5
        x *= 2.
        return x


def inst_class(*args, **kwargs):
    return InceptionV3(*args, **kwargs)
