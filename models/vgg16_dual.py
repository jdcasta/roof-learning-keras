from os import path

from keras.applications.vgg16 import VGG16 as KerasVGG16
from keras.models import Model
from keras.layers import Flatten, Dense, Dropout, Add, Concatenate, Activation
from keras.activations import softmax

import config
from .base_model import BaseModel

from .vgg16 import VGG16


class VGG16_Dual(BaseModel):

    def __init__(self, *args, **kwargs):
        super(VGG16_Dual, self).__init__(*args, **kwargs)

    def load_weights_individual(self, weights=None):
        " Load model from file for continued training "
        print('Loading indivdual rgb/lidar models from saved file')
        rgb_weights = weights[0] if weights else config.get_fine_tuned_weights_path(trunk='rgb')
        lidar_weights = weights[1] if weights else config.get_fine_tuned_weights_path(trunk='lidar')
        if path.exists(rgb_weights):
            self.model.load_weights(rgb_weights, by_name=True)
        if path.exists(lidar_weights):
            self.model.load_weights(lidar_weights, by_name=True)

    def load_weights(self, weights=None):
        print("Creating model and loading weights")
        self._create()
        if weights and len(weights) == 1:
            print('Loadings Specified Model Weights for full Dual Model')
            self.model.load_weights(weights[0], by_name=True)    
        elif path.exists(config.get_fine_tuned_weights_path()):
            print('Loading Previous Full Dual Model Weights')
            # If fine tuned weights for dual model exists
            self.model.load_weights(config.get_fine_tuned_weights_path(), by_name=True)
        else:
            # Load the weights from individual trunks
            self.load_weights_individual(weights=weights)
        self.load_classes()
        return self.model

    def _create(self):
        base_model_rgb, x_rgb = VGG16._create_base(
            self.get_input_tensor(), 'rgb')
        
        x_rgb = Dense(len(config.classes), activation='softmax',
                    name='rgb_predictions')(x_rgb)

        base_model_lidar, x_lidar = VGG16._create_base(
            self.get_input_tensor(), 'lidar')

        x_lidar = Dense(len(config.classes), activation='softmax',
                    name='lidar_predictions')(x_lidar)

        base_model = Concatenate(name=config.data_sub_dir + '_concatenate')([x_rgb, x_lidar])

        # predictions = Activation(softmax, name=config.data_sub_dir + '_predictions')(base_model)

        x = base_model
        # x = Flatten(name=config.data_sub_dir + '_flatten')(x)
        # x = Dense(self.noveltyDetectionLayerSize, activation='elu',
        #           name=config.data_sub_dir + '_fc2')(x)
        # x = Dropout(0.6, name=config.data_sub_dir + '_dropout')(x)
        predictions = Dense(len(config.classes), activation='softmax',
                            name=config.data_sub_dir + '_predictions')(x)

        self.model = Model(inputs=[base_model_rgb.input, base_model_lidar.input], outputs=predictions)


def inst_class(*args, **kwargs):
    return VGG16_Dual(*args, **kwargs)
