from os import path
from keras.applications.resnet50 import ResNet50 as KerasResNet50
from keras.layers import (Flatten, Dense, Dropout, Concatenate)
from keras.models import Model

import config
from .base_model import BaseModel
from .resnet50 import ResNet50

class ResNet50Dual(BaseModel):

    def __init__(self, *args, **kwargs):
        super(ResNet50Dual, self).__init__(*args, **kwargs)


    def load_weights_individual(self, weights=None):
        " Load model from file for continued training "
        print('Loading individual rgb/lidar models from saved file')
        rgb_weights = weights[0] if weights else config.get_fine_tuned_weights_path(trunk='rgb')
        lidar_weights = weights[1] if weights else config.get_fine_tuned_weights_path(trunk='lidar')
        if path.exists(rgb_weights):
            self.model.load_weights(rgb_weights, by_name=True)
        if path.exists(lidar_weights):
            self.model.load_weights(lidar_weights, by_name=True)

    def load_weights(self, weights=None):
        print("Creating model and loading weights")
        self._create()
        if weights and len(weights) == 1:
            print('Loadings Specified Model Weights for full Dual Model')
            self.model.load_weights(weights[0], by_name=True)    
        elif path.exists(config.get_fine_tuned_weights_path()):
            print('Loading Previous Full Dual Model Weights')
            # If fine tuned weights for dual model exists
            self.model.load_weights(config.get_fine_tuned_weights_path(), by_name=True)
        else:
            # Load the weights from individual trunks
            self.load_weights_individual(weights=weights)
        # self.load_classes()
        return self.model


    def _create(self):
        base_model_rgb, x_rgb = ResNet50._create_base(
            self.get_input_tensor(), 'rgb')
        
        base_model_lidar, x_lidar = ResNet50._create_base(
            self.get_input_tensor(), 'lidar')
            

        base_model = Concatenate(name=config.data_sub_dir + '_concatenate')([x_rgb, x_lidar])
        x = base_model
        x = Dense(config.FC2_SIZE, activation='elu',
                  name=config.data_sub_dir + '_fc2')(x)
        x = Dropout(0.5, name=config.data_sub_dir + '_dropout')(x)

        predictions = Dense(len(config.classes), activation='softmax',
                    name=config.data_sub_dir + '_predictions')(x)
        self.model = Model(inputs=[base_model_rgb.input, base_model_lidar.input], outputs=predictions)

        return self.model


def inst_class(*args, **kwargs):
    return ResNet50Dual(*args, **kwargs)
