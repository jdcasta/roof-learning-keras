from keras.applications.inception_resnet_v2 import InceptionResNetV2 as KerasInceptionResnetV2
from keras.layers import (Flatten, Dense, Dropout, AveragePooling2D, Lambda)
from keras.models import Model

import config
from .base_model import BaseModel


class InceptionResentV2(BaseModel):

    def __init__(self, *args, **kwargs):
        super(InceptionResentV2, self).__init__(*args, **kwargs)
        # if not self.freeze_layers_number:
            # we chose to train the top 2 identity blocks and 1 convolution block
            # self.freeze_layers_number = 80


    @staticmethod
    def _create_base(input_tensor, base_name, pooling='avg'):
        " Creates the input and output layers (before softmax) for this model "
        base_model = KerasInceptionResnetV2(include_top=False, input_tensor=input_tensor, pooling=pooling)
        BaseModel.rename_layers(base_model, base_name)
        BaseModel.make_net_layers_non_trainable(base_model)

        x = base_model.output
        x = Lambda(lambda x: x, name=base_name + '_features')(x)
        if config.FC1_SIZE != 0:
            x = Dense(config.FC1_SIZE, activation='elu',
                        name=base_name + '_fc1')(x)
            x = Dropout(0.5, name=base_name + '_dropout')(x)

        return (base_model, x)

    def _create(self):
        base_model, x = InceptionResentV2._create_base(
            self.get_input_tensor(), config.data_sub_dir, pooling=self.pooling)
            
        predictions = Dense(len(config.classes), activation='softmax',
                            name=config.data_sub_dir + '_predictions')(x)

        self.model = Model(inputs=base_model.input, outputs=predictions)

        return self.model


def inst_class(*args, **kwargs):
    return InceptionResentV2(*args, **kwargs)
