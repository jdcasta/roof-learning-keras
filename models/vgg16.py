from keras.applications.vgg16 import VGG16 as KerasVGG16
from keras.models import Model
from keras.layers import Flatten, Dense, Dropout

import config
from .base_model import BaseModel


class VGG16(BaseModel):

    def __init__(self, *args, **kwargs):
        super(VGG16, self).__init__(*args, **kwargs)

    @staticmethod
    def _create_base(input_tensor, base_name):
        " Creates the input and output layers (before softmax) for this model "
        base_model = KerasVGG16(
            weights='imagenet', include_top=False, input_tensor=input_tensor)
        BaseModel.rename_layers(base_model, base_name)
        BaseModel.make_net_layers_non_trainable(base_model)

        x = base_model.output
        x = Flatten(name=base_name + '_flatten')(x)
        x = Dense(config.FC1_SIZE, activation='elu',
                  name=base_name + '_fc1')(x)
        x = Dropout(0.5, name=base_name + '_dropout')(x)

        return (base_model, x)

    def _create(self):
        base_model, x = VGG16._create_base(
            self.get_input_tensor(), config.data_sub_dir)

        predictions = Dense(len(config.classes), activation='softmax',
                            name=config.data_sub_dir + '_predictions')(x)

        self.model = Model(inputs=base_model.input, outputs=predictions)

        return self.model


def inst_class(*args, **kwargs):
    return VGG16(*args, **kwargs)
