from os import path
import math

from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.preprocessing import image
from keras.applications.imagenet_utils import preprocess_input as preprocess_input_vgg16
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import Input
from keras.optimizers import SGD, Adadelta, Adam
from keras.models import load_model
from keras import backend as K
import numpy as np
from sklearn.externals import joblib

import config
import util


class BaseModel(object):
    def __init__(self,
                 class_weight=None,
                 nb_epoch=1000,
                 freeze_layers_number=None,
                 initial_epoch=0,
                 pooling='avg'):
        self.model = None
        self.data_generator = None
        self.class_weight = class_weight
        self.nb_epoch = nb_epoch
        self.initial_epoch = initial_epoch
        self.pooling = pooling
        self.fine_tuning_patience = config.PATIENCE
        self.freeze_layers_number = freeze_layers_number
        self.img_size = (224, 224)
        self.train_gen = None
        self.valid_gen = None

    @staticmethod
    def rename_layers(model, prepend=''):
        for layer in model.layers:
            layer.name = prepend + '_' + layer.name

    def _create(self):
        raise NotImplementedError('subclasses must override _create()')

    @staticmethod
    def preprocess_input(x):
        return preprocess_input_vgg16(x)

    def _fine_tuning(self):
        # print(self.freeze_layers_number)
        self.freeze_top_layers()
        # optimizer = Adadelta(lr=config.LR, rho=0.95, epsilon=1e-08, decay=0.0)
        optimizer = Adam(lr=config.LR)

        self.model.compile(
            loss='categorical_crossentropy',
            optimizer=optimizer,
            metrics=['accuracy'])
        # IF we have not already created our generators
        if not self.valid_gen and not self.train_gen:
            self.train_gen = util.image_generator_df(config.df_train, config.data_dir, modality=config.data_sub_dir, target_size=self.img_size,
                                            batch_size=config.nb_batch_size, preprocess_input=self.preprocess_input, seed=1)
            self.valid_gen = util.image_generator_df(config.df_val, config.data_dir, modality=config.data_sub_dir, target_size=self.img_size,
                                            batch_size=config.nb_batch_size, preprocess_input=self.preprocess_input, seed=1, shuffle=False, augment=False)

        return self.fit_model()

    def fit_model(self):
        # K.set_learning_phase(1)
        history = self.model.fit_generator(
            generator=self.train_gen,
            steps_per_epoch=int(config.picture_cycles * config.nb_train_samples // config.nb_batch_size),
            epochs=self.nb_epoch,
            initial_epoch=self.initial_epoch,
            validation_data=self.valid_gen,
            validation_steps=math.ceil(config.nb_validation_samples / config.nb_batch_size),
            callbacks=self.get_callbacks(config.get_fine_tuned_weights_path(), patience=self.fine_tuning_patience),
            class_weight=self.class_weight)
        return history



    def train(self, save_full_model=True):
        if not self.model:
            print("Creating model...")
            self._create()
            print("Model is created")
        print("Fine tuning...")
        history = self._fine_tuning()
        # self.save_classes()
        if save_full_model:
            self.model.save(config.get_model_path())
        return history

    def load_model(self):
        " Load model from file for continued training "
        print('Loading model from saved file')
        if path.exists(config.get_model_path()):
            self.model = load_model(config.get_model_path())
            if path.exists(config.get_fine_tuned_weights_path()):
                print('Loading best fine-tuned weights')
                self.model.load_weights(config.get_fine_tuned_weights_path(), by_name=True)
        else:
            print('Model file not found, attempting to create model and load fine-tuned weights')
            self.load_weights()
        # self.load_classes()

    def load_weights(self, weights=None):
        " Create new model and load only weights "
        print("Creating model and loading weights")
        self._create()
        self.model.load_weights(weights[0] if weights else config.get_fine_tuned_weights_path(), by_name=True)
        # self.load_classes()
        return self.model

    @staticmethod
    def save_classes():
        joblib.dump(config.classes, config.get_classes_path())

    def get_input_tensor(self):
        return Input(shape=self.img_size + (3,))

    def save_model(self):
        self.model.save(path.join(config.models_dir, '{}_{}.h5'.format(config.model, config.data_sub_dir) ))

    @staticmethod
    def make_net_layers_non_trainable(model):
        for layer in model.layers:
            layer.trainable = False

    def freeze_top_layers(self):
        if self.freeze_layers_number:
            print("Freezing {} layers".format(self.freeze_layers_number))
            for layer in self.model.layers[:self.freeze_layers_number]:
                layer.trainable = False
            for layer in self.model.layers[self.freeze_layers_number:]:
                layer.trainable = True

    @staticmethod
    def get_callbacks(weights_path, patience=5, monitor='val_acc'):
        early_stopping = EarlyStopping(verbose=1, patience=patience, monitor='val_loss')
        model_checkpoint = ModelCheckpoint(weights_path, save_best_only=True, save_weights_only=True, monitor=monitor)
        return [early_stopping, model_checkpoint]


    @staticmethod
    def load_classes():
        config.classes = joblib.load(config.get_classes_path())

    # def load_img(self, img_path):
    #     img = image.load_img(img_path, target_size=self.img_size)
    #     x = image.img_to_array(img)
    #     x = np.expand_dims(x, axis=0)
    #     return preprocess_input(x)[0]

