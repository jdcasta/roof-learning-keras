" Performs encodings on a specific subdirectory of pictures using NN Model "
import os
import time
import argparse
import json
from os.path import join as join_path
import math
import itertools as it
from pprint import pprint as pp

import config

import numpy as np
import pandas as pd
from sklearn.externals import joblib
import keras
from keras import backend as K
from keras.models import Model

# keras.backend.set_image_data_format('channels_last')

from train import set_up_model

# keras.backend.set_image_data_format('channels_last')

np.random.seed(1337)  # for reproducibility

import config
import util

# overwrite the trained directory
TRAINED_DIR = 'multi'
config.trained_dir = join_path(config.trained_dir, TRAINED_DIR)
# print(config.trained_dir)
config.update_paths()


def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-i',
        '--input',  help='Input File for Multi Encodings', default=None, type=str, required=True)
    args = parser.parse_args()
    return args



def encode2(model_module, modality='rgb', train_val_test='test', augment=True):
    
    # classes_in_keras_format = util.get_classes_in_keras_format()
    # import pdb;pdb.set_trace()
    df = util.get_subset_df(config.df, train_val_test)
    test_generator =util.image_generator_df(df, config.data_dir, modality=modality, target_size=model_module.img_size, batch_size=config.nb_batch_size, augment=augment, preprocess_input=model_module.preprocess_input, shuffle=False)
    picture_cycles = config.picture_cycles if augment else 1
    num_samples = df.shape[0] * picture_cycles

    # Get last layer
    feature_length = model_module.model.layers[-1].output.shape[1].value

    # The true labels and predicted labels
    y_trues = np.zeros(num_samples)
    features = np.zeros(shape=(num_samples, feature_length))
    # predictions = np.zeros(num_samples)
    # File names for debugging purposes
    file_names = []
    num_loops = math.ceil(num_samples / config.nb_batch_size)
    num_samples_completed = 0
    for i, (inputs, classes) in enumerate(test_generator):
        # print(len(inputs))
        # print(inputs[0].shape, inputs[1].shape)

        # calculate the indexes
        this_batch_size = classes.shape[0]
        n_from = num_samples_completed
        n_to = num_samples_completed + this_batch_size
        num_samples_completed += this_batch_size
        # print("Batch: {}; n_from: {}, n_to: {}".format(i, n_from, n_to))
        y_true = np.argmax(classes, axis=1)
        y_trues[n_from:n_to] = y_true

        file_name = list(test_generator.original.filenames[n_from: n_to])
        file_names += file_name
        # print(file_name)

        # Make predictions
        start = time.clock()
        feature = model_module.model.predict(inputs)
        features[n_from:n_to, :] = feature
        end = time.clock()
        print('Feature Extraction on batch {} took: {}'.format(i, end - start))

        if num_samples_completed >= num_samples:
            break


    return (features, y_trues, np.array(file_names))


def extract_feature_model(model_module, base_name='rgb', suffix='_features'):
    """ 
    Given any model, extract the features from the Model. 
    Instead of a prediciton of class, give the fully connected layer
    """
    model = model_module.model
    layer_name = base_name + suffix
    feature_layer_model = Model(inputs=model.input,
                                outputs=model.get_layer(layer_name).output)
    fc_shape = feature_layer_model.output_shape
    model_module.model = feature_layer_model
    return model_module, fc_shape


def run_encode2(output_file_path=None, train_val_test='test', augment=True, **kwargs):
    model_module = set_up_model(**kwargs)
        # Cut off the top of the model (the predictions), just get the condensed feature vector
    model_module, fc_shape = extract_feature_model(model_module, base_name=config.data_sub_dir)
    (features, labels, file_names) = encode2(model_module, modality=config.data_sub_dir, train_val_test=train_val_test, augment=augment)
    
    # df = pd.DataFrame({"features": features, "labels": labels, "file_names": file_names})

    joblib.dump({'features': features, 'labels': labels, 'file_names': file_names}, output_file_path)
    # df.to_pickle(output_file_path)

    K.clear_session()
    del model_module

    
def create_tests(test_sets):
    test_sets_full = []
    # Iterate through each model test set
    for test_set in test_sets:
        new_test = dict(test_set) # make a copy of it

        hyperparameters = new_test['hyperparameters']
        hyperparameter_list = util.dictproduct(hyperparameters)
        for hyper_set in hyperparameter_list:
            full_test = dict(new_test)
            full_test.update(hyper_set)      # add the hyperparameters
            del full_test['hyperparameters'] # dont need this anymore
            # full_test['data_dir'] = join_path(full_test['data_dir'], full_test['encode_sub_dir'])
            full_test['output_file_name'] = "_".join([full_test['train_val_test'], full_test['model'], full_test['data_sub_dir'], full_test['dataset']]) + ".pkl"
            full_test['output_file_path'] = os.path.join(full_test['output_dir'], full_test['output_file_name'])
            full_test['augment'] = full_test['train_val_test'] == 'train'
            test_sets_full.append(full_test)

    return test_sets_full

def main():
    " Main Function in script "
    # input('Wait for debug')
    tic = time.clock()

    args = parse_args()
    print('=' * 50)
    print('Called with args:')
    print(args)
    if args.input:
        # encode_tests =generate_encode_tests(args.input)
        test_files, name = util.read_test_file(args.input)
        # pp(test_files)
        encode_tests = create_tests(test_files)
        # pp(encode_tests)
        for encode_test in encode_tests:
            run_encode2(**encode_test)
    else:
        print('Need input file!')

if __name__ == '__main__':
    main()
