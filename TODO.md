# TODO

- Remove dependence on validation directory
- Should have a train directory and a test directory
- Make sure to plug in my defined generator for data


Commmand line parameters

Split Pictrues:
python scripts/data_split.py --data_dir /media/jeremy/TOSHIBA\ EXT/Research/roof_learning/witten/pics/all --dest_dir /media/jeremy/TOSHIBA\ EXT/Research/roof_learning/witten/pics --split 60,30,10
Training:
python train.py --data_dir /media/jeremy/TOSHIBA\ EXT/Research/roof_learning/witten/pics --model vgg16 --data_sub_dir rgb  
Testing: 
python predict.py --data_dir /media/jeremy/TOSHIBA\ EXT/Research/roof_learning/witten/pics/valid --model vgg16 --data_sub_dir rgb

Encoding:
python encode.py --data_dir imgs/pics/valid --data_sub_dir rgb --model resnet50 --weights trained/multi/fine-tuned-resnet50_lr_0.0001_fc1_50_fc2_100_layer_80_run_0_rgb-rgb-weights.h5 --batch_size 48 --output valid_resnet_rgb.pkl


Dual Input Training
python train.py --data_dir /media/jeremy/TOSHIBA\ EXT/Research/roof_learning/witten/pics/valid --model vgg16_dual --data_sub_dir rgb --weights trained/multi/fine-tuned-vgg16_lr_1e-05_fc1_50_fc2_100_layer_11_run_0-rgb-weights.h5 trained/multi/fine-tuned-vgg16_lr_1e-05_fc1_50_fc2_100_layer_11_run_0-lidar-weights.h5 



Do the rotation -
Don't recreate generators - Done
Run a few test and see if it works
Analyse the history returns


Models - VGG16, Resnet50, Inception_v3

Inception Layers Groups-
          280, 249, 229, 197, 165, 133, 101, 87, 64, 41, 18
Resenet
          80, 70, 60, 50


Use scratch directory for pictures

# Best Models

Best RGB Model
class_modifier                                                 none
dataset                                                    combined
fc1                                                             100
fc1_plot                                                        Yes
full_name         inceptionresnetv2_dataset_combined_lr_0.001_fc...
input                                                           rgb
layer                                                            11
model                                             inceptionresnetv2
run                                                               0
time                                                        146.419
train_acc                                                  0.781783
val_acc                                                    0.780069
val_loss                                                   0.705919
Name: 16, dtype: object


Best LIDAR Model
class_modifier                                                 none
dataset                                                    combined
fc1                                                               0
fc1_plot                                                         No
full_name         resnet50_dataset_combined_lr_0.001_fc1_0_fc2_1...
input                                                         lidar
layer                                                            80
model                                                      resnet50
run                                                               0
time                                                        36.0236
train_acc                                                  0.880064
val_acc                                                    0.883133
val_loss                                                   0.390863
Name: 28, dtype: object


## Domain Specific

## LIDAR

Using Validation Set:
[{'name': 'resnet50_lidar_witten_witten', 'val_acc': 0.8850102669404517},
 {'name': 'resnet50_lidar_witten_combined', 'val_acc': 0.9096509240246407},
 {'name': 'resnet50_lidar_newyork_newyork', 'val_acc': 0.8768328445747801},
 {'name': 'resnet50_lidar_newyork_combined', 'val_acc': 0.8425655976676385}]

Using Test Data Set:
[{'name': 'resnet50_lidar_witten_witten', 'val_acc': 0.831013916500994},
 {'name': 'resnet50_lidar_witten_combined', 'val_acc': 0.8469184890656064},
 {'name': 'resnet50_lidar_newyork_newyork', 'val_acc': 0.8764705882352941},
 {'name': 'resnet50_lidar_newyork_combined', 'val_acc': 0.8973607038123167}]

 ## RGB
Using Validation Set

[{'name': 'inceptionresnetv2_rgb_witten_witten', 'val_acc': 0.683111954459203},
 {'name': 'inceptionresnetv2_rgb_witten_combined','val_acc': 0.7438330170777988},
 {'name': 'inceptionresnetv2_rgb_newyork_newyork','val_acc': 0.7703488372093024},
 {'name': 'inceptionresnetv2_rgb_newyork_combined','val_acc': 0.8352601156069365}]

 Using Test Data Set:
 [{'name': 'inceptionresnetv2_rgb_witten_witten', 'val_acc': 0.6468330134357005},
 {'name': 'inceptionresnetv2_rgb_witten_combined', 'val_acc': 0.72552783109405},
 {'name': 'inceptionresnetv2_rgb_newyork_newyork', 'val_acc': 0.749271137026239},
 {'name': 'inceptionresnetv2_rgb_newyork_combined','val_acc': 0.7936046511627907}]




## Rewrite

### Resnet 50

Change to Layer 38 or 39 to get the finish stage 2 the
Stage 1 - Layer 6
Stage 2 - Layer 39
Stage 3-  Layer 80


Need to add this layer - `layers.AveragePooling2D((7, 7), name='avg_pool')(x)`

Upgrading to keras changed the layers

Wonder if global max will suffice?

### InceptionV3

First Convs - Layer 11
Seconds Convs - Layer 18
First Mixed - Layer 41
Second Mixed - Layer 64
Third Mixed - Layer 86


### IncecptionResnetV2

First COnvs - Layer 11
Seconds Convs - Layer 18
First Mixed - Layer 41
Second MIxed - 275



### Image Generator 

The image generator needs to change.  It should be reading from a loaded csv file that has information about each picture and its corresponding category and image source for each modality.

keywords - `data_dir, classes, sub_dir, nb_train_samples, nb_validation_samples, nb_test_samples`

Places to change this:
2. `util.py` - image_generator, dual_image_generator - This actually might just be the main place I need to change this.
1. `train.py` - Line 61 Starting
2. `config.py` - set_paths
3. `models/base_model.py` - def _fine_tuning(self)
4. `models/resent50.py`-  config.data_sub_dir


Looks like I should Extend from Base Iterator Class


#### Why

Before every building had two images that were classified the **exact** same.  This made it practical to use the built in Keras Image Generator. This is no longer the case as some buildings will have a bad image in one modality but a good image in another (different classes).  We will need to modify the image generator to handle this, especially during dual input.


## Notes from Ella for paper

Fix graph for Precison vs Recall, no titles

Make another graph for validation set accuracy for the swarm plot, make sure that its similar

Confusion matrix - 2X2 whenever you break up the flat-like crap


