import os
import sys
sys.path.insert(0, os.getcwd())

from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix, classification_report
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from notebookutils import load_data, calculate_metrics
import util
import config

sns.set(font_scale=1.2)  # crazy big


def make_swarm_plot(df, name='test_data_set_classical.pdf'):
    with sns.axes_style("darkgrid"):
        current_palette = sns.color_palette()
        custom_palette = {"InceptionResnetV2": current_palette[1], "Resnet50": current_palette[2], "SVM": current_palette[3], "Random Forest": current_palette[4]}

        ax = sns.swarmplot(x="input", y="test_acc", hue="name", data=df, palette=custom_palette)
        # fig = plt.gcf()
        # fig.legend(loc=7)
        # ax.legend_.set_title("Models")
        ax.legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
        ax.set_ylim(.65, .90)
        # ax._legend.set_bbox_to_anchor((.76, .35))
        ax.set(xlabel='Input', ylabel='Test Set Accuracy', title='')
        plt.savefig('results/{}'.format(name), bbox_inches="tight")

nn_results = [
    {
        "input": 'rgb',
        'name': 'InceptionResnetV2',
        'params': str({'fc1': 100}),
        "all_acc": 0,
        'test_acc': 0.72,  # comes from predict.py test-.84,  all - .73
        'train_acc': .7817,  # comes from mutli train file
        'valid_acc': .7800  # comes from mutli train file
    },
    {
        "input": 'lidar',
        'name': 'Resnet50',
        'params': str({'fc1': 0}),
        "all_acc": 0,
        'test_acc': 0.82,      # comes from predict.py test-.95,  all-.87
        'train_acc': 0.880,  # comes from mutli train file
        'valid_acc': 0.883  # comes from mutli train file
    }
]


# Will hold the classes after analyzing train_dir, default class names overided
classes = ["complex", "complex_flat", "flat", "gabled", "half-hipped", "hipped", "pyramidal", "skillion"]
# Get all the classifiers
all_results= joblib.load('results/saved_classifiers_RERUN.pkl')
dual_results = [result for result in all_results if result['input'] == 'dual'] 
lidar_results = [result for result in all_results if result['input'] == 'lidar'] 
rgb_results = [result for result in all_results if result['input'] == 'rgb'] 
# Get the best dual classifier
best_dual_classifier = max(dual_results, key=lambda x:x['test_acc'])
best_lidar_classifier = max(lidar_results, key=lambda x:x['test_acc'])
best_rgb_classifier = max(rgb_results, key=lambda x:x['test_acc'])

# Get Test Data
test_data_lidar = "trained/encoded/test_resnet50_lidar_combined.pkl"
test_data_rgb = "trained/encoded/test_inceptionresnetv2_rgb_combined.pkl"
xl_test, fl_test, yl_test = load_data(test_data_lidar, shuffle=False)
xr_test, fr_test, yr_test = load_data(test_data_rgb, shuffle=False)
xd_test = np.concatenate((xl_test, xr_test), axis=1)


# Plot Swarm Plot
df = pd.DataFrame.from_dict(all_results)
df = df.append(nn_results, ignore_index=True)

make_swarm_plot(df)
df = df.drop('classifier', axis=1)
# print(df.columns)
df.to_csv('results/classical_training_results_RERUN.csv')


# Run best dual classifier through test data set
yd_pred = best_dual_classifier['classifier'].predict(xd_test)
cf = confusion_matrix(yl_test, yd_pred)
report = classification_report(yl_test, yd_pred)
print("Classification Report \n")
print(report)

# util.plot_confusion_matrix(cf, classes, accuracy=best_dual_classifier['test_acc'], normalize=False, output='dual_rf_test', dataset='combined')
# util.plot_confusion_matrix(cf, classes, accuracy=best_dual_classifier['test_acc'], normalize=True, output='dual_rf_test', dataset='combined')
# print("Metrics Report \n")
# print(calculate_metrics(cf))
