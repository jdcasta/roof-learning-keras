from multiprocessing import Pool
import os
import itertools
from math import trunc

from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from ast import literal_eval as make_tuple
from sklearn.externals import joblib
from sklearn.utils.class_weight import compute_class_weight
from collections import OrderedDict

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style()


models_all = {
    "class_weight": 'balanced'
}
NUM_CORES = 6


def plot_confusion_matrix(cm, classes,
                          accuracy,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues,
                          sub_dir=None,
                          output='',
                          dataset='UK'):
    """
    This function prints and plots the confusion matrix.
    """
    with sns.axes_style("white"):
        confusion_matrix_dir = './confusion_matrix_plots'
        if not os.path.exists(confusion_matrix_dir):
            os.mkdir(confusion_matrix_dir)

        cm_norm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

        plt.cla()
        plt.figure()
        plt.imshow(cm_norm, interpolation='nearest', cmap=cmap)
        if accuracy is not None:
            acc_title = title + \
                ". {:d}% accuracy".format(trunc(accuracy * 100))
            plt.title(acc_title)
        plt.colorbar()
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=45)
        plt.yticks(tick_marks, classes)

        # print(cm)
        fmt = '{:.2f}\n({:d})'
        thresh = cm_norm.max() / 1.25
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            # print(cm[i,j], thresh)
            color = "#BFD1D4" if cm_norm[i, j] > thresh else "black"
            plt.text(j, i, fmt.format(cm_norm[i, j], cm[i,j]),
                     horizontalalignment="center", verticalalignment='center',
                     color=("#BFD1D4" if cm_norm[i, j] > thresh else "black"))

        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')

        end_suffix = sub_dir if sub_dir else 'dual'

        plt.savefig(os.path.join(confusion_matrix_dir,
                                     '{}_{}_{}.pdf'.format(output, dataset, end_suffix)), bbox_inches='tight')
        plt.cla()
        plt.clf()

def calculate_metrics(cf):
    FP = cf.sum(axis=0) - np.diag(cf)  
    FN = cf.sum(axis=1) - np.diag(cf)
    TP = np.diag(cf)
    TN = cf.sum() - (FP + FN + TP)
    
    # Sensitivity, hit rate, recall, or true positive rate
    TPR = TP/(TP+FN) # Recall, sensitivity, hit rate
    PPV = TP/(TP+FP) # Precision

    # Precision or positive predictive value
    QUAL = TP/(TP+FP+FN)
    matrix = np.column_stack((TPR.transpose(), PPV.transpose(), QUAL.transpose()))
    
    return matrix

def randomize(features, files, labels):
    # Generate the permutation index array.
    permutation = np.random.permutation(features.shape[0])
#     print(permutation)
    # Shuffle the arrays by giving the permutation in the square brackets.
    features_s = features[permutation]
    files_s = files[permutation]
    labels_s = labels[permutation]
    return features_s, files_s, labels_s

def load_data(file_name, shuffle=True):
    data_set = joblib.load(file_name)
    features, files, labels = data_set['features'], data_set['file_names'], data_set['labels']
    return randomize(features, files, labels) if shuffle else (features, files, labels)

def get_name(name):
    if name == 'SVC':
        return 'SVM'
    elif name == 'RandomForestClassifier':
        return 'Random Forest'

def dictproduct(dct):
    for t in itertools.product(*dct.values()):
        default = dict(models_all)
        params = dict(zip(dct.keys(), t))
        default.update(params)
        yield default

def create_tasks(tasks):
    result = [{v: vv} for v in tasks for vv in dictproduct(tasks[v])]
    return result

def fit_data(clf_name, clf_params, x_train, y_train):
    classifier = globals()[clf_name](**clf_params)
    classifier.fit(x_train, y_train)
    return classifier

class JobRunner(object):
    def __init__(self, x_t, y_t, x_v, y_v, x_test, y_test, tasks, inputs='rgb', x_all=None, y_all=None):
        " x = feautres, y = labels, t = train, v = validation, test = test, all = all data (optional)"
        self.x_t = x_t
        self.y_t = y_t
        self.x_v = x_v
        self.y_v = y_v
        self.x_test = x_test
        self.y_test = y_test
        self.tasks = tasks
        self.input = inputs
        self.x_all = x_all
        self.y_all = y_all
    def run_task(self, task_name, task_params):
        print(task_name, task_params)
        classifier = fit_data(task_name, task_params, self.x_t, self.y_t)
        t_accuracy = classifier.score(self.x_t, self.y_t)
        v_accuracy = classifier.score(self.x_v, self.y_v)
        test_accuracy = classifier.score(self.x_test, self.y_test)
        if self.x_all is not None and self.y_all is not None:
            all_accuracy = classifier.score(self.x_all, self.y_all)
        else:
            all_accuracy = None
        results = {
            "name": get_name(task_name),
            "params": str(task_params),
            "train_acc": t_accuracy,
            "valid_acc": v_accuracy,
            "test_acc": test_accuracy,
            "all_acc": all_accuracy,
            "input": self.input,
            "classifier": classifier
        }
#         pp(results)
        return results

    def run_tasks(self):
        fully_specified_tasks = []
        for task in self.tasks:
            task_name, task_params = next (iter (task.items()))
            fully_specified_tasks.append((task_name, task_params))
#         pp(fully_specified_tasks)
        with Pool(processes=NUM_CORES) as p:
            final_results = p.starmap(self.run_task, fully_specified_tasks)
        return final_results

def make_swarm_plot(df, name='test_data_set_classical.png'):
    with sns.axes_style("darkgrid"):
        current_palette = sns.color_palette()
        custom_palette = {"InceptionResnetV2": current_palette[1], "Resnet50": current_palette[2], "SVM": current_palette[3], "Random Forest": current_palette[4]}

        ax = sns.swarmplot(x="input", y="test_acc", hue="name", data=df, palette=custom_palette)
        ax.legend_.set_title("Models")
        # ax._legend.set_bbox_to_anchor((.76, .35))
        ax.set(xlabel='Input', ylabel='Test Set Accuracy', title='')
        plt.savefig('results/{}'.format(name))
