
from pprint import pprint as pp
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
# sns.set_style()
sns.set(font_scale=1.2)  # crazy big

def create_dict(item):
    tokens = item['name'].split('_')
    result = {
        "model": tokens[0],
        "input": tokens[1],
        "dataset": tokens[2],
        "training": "Region Specific" if tokens[3] == tokens[2] else "Combined Datasets",
        'val_acc': item['val_acc']
    }
    return result

def make_grouped_bar_cnn(df, input_type='rgb', name='multi_data_set'):
    df_filter = df[df.input == input_type]
    # df_filter = df_filter.groupby(['model', 'fc1_plot'])['val_acc'].max().to_frame().reset_index(level=['model', 'fc1_plot'])
    # import pdb; pdb.set_trace()
    # print(best_val_accs)
    # df_filter = df_filter[df_filter.val_acc.isin(best_val_accs.values)]
    # print(df_filter)
    with sns.axes_style("darkgrid"):
        current_palette = sns.color_palette()
        custom_palette = {"Region Specific": current_palette[0], "Combined Datasets": current_palette[1]}

        g = sns.catplot(x="dataset", y="val_acc", hue="training", data=df_filter, kind="bar", palette=custom_palette, legend_out=False)
        # ax = sns.swarmplot(x="fc1_plot", y="val_acc", hue="model", data=df_filter, palette=custom_palette)
        ax = g.facet_axis(0, 0)
        # import pdb; pdb.set_trace()
        # ax.legend_.set_title("Models")
        # ax._legend.set_bbox_to_anchor((.76, .35))
        ax.set(xlabel='Validation Dataset', ylabel='Region Validation Set Accuracy', title='')
        ax.set_ylim(.65, 1.0)
        # ax.set_title("{}".format(input_type.upper()))
        plt.savefig('results/{}_{}.pdf'.format(name, input_type))
        # ax.set_ylim(.60, 1.0)
    plt.cla()
    plt.figure()

def main():
    multi_data = [{'name': 'inceptionresnetv2_rgb_witten_witten', 'val_acc': 0.683111954459203},
                {'name': 'inceptionresnetv2_rgb_witten_combined',
                'val_acc': 0.7438330170777988},
                {'name': 'inceptionresnetv2_rgb_newyork_newyork',
                'val_acc': 0.7703488372093024},
                {'name': 'inceptionresnetv2_rgb_newyork_combined',
                'val_acc': 0.8352601156069365},
                {'name': 'resnet50_lidar_witten_witten',
                    'val_acc': 0.8850102669404517},
                {'name': 'resnet50_lidar_witten_combined',
                    'val_acc': 0.9096509240246407},
                {'name': 'resnet50_lidar_newyork_newyork',
                    'val_acc': 0.8768328445747801},
                {'name': 'resnet50_lidar_newyork_combined', 'val_acc': 0.8425655976676385}]
    
    multi_data = [create_dict(data) for data in multi_data]
    df = pd.DataFrame.from_dict(multi_data)
    df = df.sort_values('model')
    print(df)
    make_grouped_bar_cnn(df, input_type='rgb')
    make_grouped_bar_cnn(df, input_type='lidar')
if __name__ == '__main__':
    main()



