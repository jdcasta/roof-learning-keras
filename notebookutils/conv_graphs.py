# Setup, run only once!
import os
import sys
import argparse
sys.path.insert(0, os.getcwd())

import pickle
from pprint import pprint as pp
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
# sns.set_style()
sns.set(font_scale=1.2)  # crazy big


def load_files(*args):
    " Loads a series of pickel files "
    history = {}
    for file in args:
        file_history = pickle.load( open(file, "rb" ) )
        history.update(file_history)
    return history

def get_model_dict(model_name, model):
    # model_tokens = model_name.split('_')
    params = model['params']
    # print(model_name, model_tokens, model)
    # sys.exit()
    values =  {
        'full_name': model_name,
        'model': params['model'],
        'dataset': params['dataset'],
#         'lr': float(model_tokens[2]),
        'fc1': params['fc1'],
        'fc1_plot': 'Yes' if params['fc1'] != 0 else 'No',
#         'fc2': int(model_tokens[6]),
        'layer': params['freeze_layers_number'],
        'run': params['run'],
        'input': params['data_sub_dir'],
        'class_modifier':  params.get('class_modifier', 'none')
    }
    return values

def get_best_accuracy(model_name, model):
    model_acc_list = model['val_acc']
    acc = max(model_acc_list)
    index_acc = model_acc_list.index(acc)
    loss = model['val_loss'][index_acc]
    train_acc = model['acc'][index_acc]
    acc_dict = {
        'train_acc': train_acc,
        'val_acc': acc,
        'val_loss': loss,
        'time': model['time']/60.0 if 'time' in model else ''
    }
    return acc_dict

def get_model_stats(model_name, model):
    model_dict = get_model_dict(model_name, model)
    acc_dict = get_best_accuracy(model_name, model)
    model_dict.update(acc_dict)
    return model_dict

def get_best_model(df):
    inputs = df.input.unique()
    if 'rgb' in inputs:
        best_rgb = df.loc[df[df.input == 'rgb']['val_acc'].idxmax()]
        print('Best RGB Model')
        pp(best_rgb)
        print('\n')

    if 'lidar' in inputs:
        best_lidar = df.loc[df[df.input == 'lidar']['val_acc'].idxmax()]
        print('Best LIDAR Model')
        pp(best_lidar)

def plot_scatter(df, input_type='lidar'):
    current_palette = sns.color_palette()
    custom_pallete = {"inceptionv3": current_palette[0], "resnet50": current_palette[2], "inceptionresnetv2": current_palette[1]}

    df_filter = df[df.input == input_type]
    # df_rgb = df[df.input == 'rgb']
    ax = sns.pairplot(x_vars=['time'], y_vars=['val_acc'], data=df_filter, hue="model", height=5, palette=custom_pallete)
    #              plot_kws={"s": df['layer']})
    ax._legend.set_title("Models")
    ax._legend.set_bbox_to_anchor((.73, .4))
    ax.set(xlabel='Time to train (min)', ylabel='Validation Set Accuracy', title='CNN {} Results'.format(input_type))
    plt.savefig('results/cnn_{}_results.png'.format(input_type))


def make_grouped_bar_cnn(df, input_type='rgb', name='val_data_set_cnn', max_only=True):
    df_filter = df[df.input == input_type]
    df_filter = df_filter.groupby(['model', 'fc1_plot'])['val_acc'].max().to_frame().reset_index(level=['model', 'fc1_plot'])
    # import pdb; pdb.set_trace()
    # print(best_val_accs)
    # df_filter = df_filter[df_filter.val_acc.isin(best_val_accs.values)]
    # print(df_filter)
    with sns.axes_style("darkgrid"):
        current_palette = sns.color_palette()
        custom_palette = {"inceptionv3": current_palette[0], "inceptionresnetv2": current_palette[1], "resnet50": current_palette[2]}

        g = sns.catplot(x="fc1_plot", y="val_acc", hue="model", data=df_filter, kind="bar", palette=custom_palette, legend_out=False)
        # ax = sns.swarmplot(x="fc1_plot", y="val_acc", hue="model", data=df_filter, palette=custom_palette)
        ax = g.facet_axis(0, 0)
        # import pdb; pdb.set_trace()
        # ax.legend_.set_title("Models")
        # ax._legend.set_bbox_to_anchor((.76, .35))
        ax.set(xlabel='Fully Connected Layer', ylabel='Validation Set Accuracy', title='')
        ax.set_ylim(.65, 1.0)
        # ax.set_title("{}".format(input_type.upper()))
        plt.savefig('results/{}_{}.pdf'.format(name, input_type))
        # ax.set_ylim(.60, 1.0)
    plt.cla()
    plt.figure()

def make_swarm_plot_cnn(df, input_type='rgb', name='val_data_set_cnn'):
    df_filter = df[df.input == input_type]
    # df_filter = df
    with sns.axes_style("darkgrid"):
        current_palette = sns.color_palette()
        custom_palette = {"inceptionv3": current_palette[0], "inceptionresnetv2": current_palette[1], "resnet50": current_palette[2]}

        ax = sns.swarmplot(x="fc1_plot", y="val_acc", hue="model", data=df_filter, palette=custom_palette)
        ax.legend_.set_title("Models")
        # ax._legend.set_bbox_to_anchor((.76, .35))
        ax.set(xlabel='Fully Connected Layer', ylabel='Validation Set Accuracy', title='')
        ax.set_ylim(.54, 1.0)
        ax.set_title("{}".format(input_type.upper()))
        plt.savefig('results/{}_{}.pdf'.format(name, input_type))
        # ax.set_ylim(.60, 1.0)
    plt.cla()
    plt.figure()

def parse_args():
    parser = argparse.ArgumentParser(description='Process files')
    parser.add_argument('-f','--files', nargs='+', help='Specify files to load', required=False)
    parser.add_argument('-g', '--graph', action='store_true', help='Output Graphs')

    return parser.parse_args()



if __name__ == '__main__':

    args = parse_args()
    if args.files:
        print("Loading files", args.files)
        history = load_files(*args.files)
    else:
        # Files holding the history of our training of models
        # file1 = "/media/jeremy/TOSHIBA EXT/Research/roof_learning/witten/models/multi_9_2_2017/inceptionv3_single_history.pkl"
        # file2 = "/media/jeremy/TOSHIBA EXT/Research/roof_learning/witten/models/multi_9_2_2017/vgg16_single_history.pkl"
        base_dir = r"F:\jeremy\roof_learning_journal\results\models\09_06_2018"
        file1 =  os.path.join(base_dir, "inceptionv3_single_history.pkl")
        file2 =  os.path.join(base_dir, "inceptionresnetv2_single_history.pkl")
        file3 =  os.path.join(base_dir, "resnet50_single_history.pkl")
        history = load_files(file1, file2, file3)

    # print(history)
    model_dicts = [get_model_stats(key, value) for key,value in history.items()]
    # print(model_dicts)
    df = pd.DataFrame.from_dict(model_dicts)
    df = df.sort_values('model')
    # df = df[df['dataset'] == 'combined'] # Only judge best model on the combined dataset
    df.to_csv('results/multi_train.csv')        

    ## Print out the best model
    get_best_model(df)

    if args.graph:
        print("Generating Graphs...")
        make_grouped_bar_cnn(df, input_type='rgb')
        make_grouped_bar_cnn(df, input_type='lidar')
