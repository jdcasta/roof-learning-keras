import os
import sys
from pprint import pprint as pp
from collections import OrderedDict

sys.path.insert(0, os.getcwd())

import numpy as np
from sklearn.externals import joblib
from sklearn.utils.class_weight import compute_class_weight

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
# sns.set_style()
sns.set(font_scale=1.2)  # crazy big

from notebookutils import create_tasks, JobRunner, load_data

train_data_lidar = "trained/encoded/train_resnet50_lidar_combined.pkl"
valid_data_lidar = "trained/encoded/val_resnet50_lidar_combined.pkl"
test_data_lidar = "trained/encoded/test_resnet50_lidar_combined.pkl"
# all_data_lidar = "trained/all_resnet50_lidar.pkl"

train_data_rgb = "trained/encoded/train_inceptionresnetv2_rgb_combined.pkl"
valid_data_rgb = "trained/encoded/val_inceptionresnetv2_rgb_combined.pkl"
test_data_rgb = "trained/encoded/test_inceptionresnetv2_rgb_combined.pkl"
# all_data_rgb = "trained/all_resnet50_rgb.pkl"

# x = feautres, y = labels, f = file_names, l = lidar, r=rgb, t = train, v = validation, test = test
shuffle_init = False
xl_t, fl_t, yl_t = load_data(train_data_lidar, shuffle=shuffle_init)
xl_v, fl_v, yl_v = load_data(valid_data_lidar, shuffle=shuffle_init)
xl_test, fl_test, yl_test = load_data(test_data_lidar, shuffle=shuffle_init)
xl_all, yl_all = (None, None)
# xl_all, fl_all, yl_all = load_data(all_data_lidar, shuffle=shuffle_init)

xr_t, fr_t, yr_t = load_data(train_data_rgb, shuffle=shuffle_init)
xr_v, fr_v, yr_v = load_data(valid_data_rgb, shuffle=shuffle_init)
xr_test, fr_test, yr_test = load_data(test_data_rgb, shuffle=shuffle_init)
xr_all, yr_all = (None, None)
# xr_all, fr_all, yr_all = load_data(all_data_rgb, shuffle=shuffle_init)

# Concatenate RGB and LIDAR features
xd_t = np.concatenate((xl_t, xr_t), axis=1)
xd_v = np.concatenate((xl_v, xr_v), axis=1)
xd_test = np.concatenate((xl_test, xr_test), axis=1)
# xd_all = np.concatenate((xl_all, xr_all), axis=1)
xd_all = None

print("Lidar Size {}".format(xl_t.shape))
print("RGB Size {}".format(xr_t.shape))
sys.exit()


models = {
    "SVC": {
        "C": [
            1.0,
            10.0,
            100.0
        ],
        "kernel": [
            "linear",
            "rbf",
            "poly",
            "sigmoid"
        ]
    },
    "RandomForestClassifier":
    {
        "criterion": [
            "gini",
            "entropy"
        ],
        "max_depth": [
            5,
            10,
            50
        ],

        "n_estimators": [
            5,
            10,
            50
        ]
    }
}

if __name__ == '__main__':
    tasks = create_tasks(models)
    lidar_results, rgb_results, dual_results, nn_results = ([], [], [], [])

    job_runner = JobRunner(xl_t, yl_t, xl_v, yl_v, xl_test,
                           yl_test, tasks, inputs='lidar', x_all=xl_all, y_all=yl_all)
    lidar_results = job_runner.run_tasks()

    job_runner = JobRunner(xr_t, yr_t, xr_v, yr_v, xr_test,
                           yr_test, tasks, inputs='rgb', x_all=xr_all, y_all=yr_all)

    rgb_results = job_runner.run_tasks()

    job_runner = JobRunner(xd_t, yl_t, xd_v, yl_v, xd_test,
                           yl_test, tasks, inputs='dual', x_all=xd_all, y_all=yl_all)

    dual_results = job_runner.run_tasks()


    all_results = rgb_results + lidar_results + dual_results  # + nn_results
    df = pd.DataFrame.from_dict(all_results)
    # print(df)

    # current_palette = sns.color_palette()
    # custom_palette = {"SVM": current_palette[1], "Resnet50": current_palette[2], "Random Forest": current_palette[0]}

    # ax = sns.swarmplot(x="input", y="test_acc", hue="name", data=df, palette=custom_palette)
    # ax.legend_.set_title("Models")
    # # ax._legend.set_bbox_to_anchor((.76, .35))
    # ax.set(xlabel='Input', ylabel='Test Set Accuracy', title='')
    # plt.savefig('results/test_data_set_classical.png')

    joblib.dump(all_results, 'results/saved_classifiers.pkl')
