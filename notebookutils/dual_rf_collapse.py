import os
import sys
import argparse
from pprint import pprint as pp
sys.path.insert(0, os.getcwd())

from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from notebookutils import load_data, calculate_metrics, plot_confusion_matrix
# import util
np.set_printoptions(precision=3, suppress=True)
sns.set_style()

mapping_flatvsall = {
    "name": "flatvsall",
    "integer_mapping": {
        0: 0,
        1: 1,
        2: 1,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
        7: 0
    },
    "shape": 2,
    "labels": {
        0: "unknown",
        1: "flat-like"
    }
}

mapping_collapseflat_aa = {
    "name": "collapseflat_aa",
    "integer_mapping": {
        0: 0,
        1: 1,
        2: 1,
        3: 2,
        4: 3,
        5: 4,
        6: 0,
        7: 5
    },
    "shape": 6,
    "labels": {
        0: "unknown",
        1: "flat-like",
        2: "gabled",
        3: "half-hipped",
        4: "hipped",
        5: "skillion"
    },
    "linegraph": {
        "classes": ['flat-like', 'gabled', 'hipped']
    }
}

mapping_collapseflat = {
    "name": "collapseflat",
    "integer_mapping": {
        0: 0,
        1: 1,
        2: 1,
        3: 2,
        4: 3,
        5: 4,
        6: 5,
        7: 6
    },
    "shape": 7,
    "labels": {
        0: "unknown",
        1: "flat-like",
        2: "gabled",
        3: "half-hipped",
        4: "hipped",
        5: "pyramidal",
        6: "skillion"
    }
}

allclasses_aa = {
    "name": "allclasses_aa",
    "integer_mapping": {
        0: 0,
        1: 1,
        2: 2,
        3: 3,
        4: 4,
        5: 5,
        6: 6,
        7: 6
    },
    "shape": 7,
    "labels": {
        0: "unknown",
        1: "complex-flat",
        2: "flat",
        3: "gabled",
        4: "half-hipped",
        5: "hipped",
        6: "skillion"
    }
}

mapping_none = {
    "name": "allclasses",
    "integer_mapping": {
        0: 0,
        1: 1,
        2: 2,
        3: 3,
        4: 4,
        5: 5,
        6: 6,
        7: 7
    },
    "shape": 8,
    "labels": {
        0: "unknown",
        1: "complex-flat",
        2: "flat",
        3: "gabled",
        4: "half-hipped",
        5: "hipped",
        6: "pyramidal",
        7: "skillion"
    }
}

MAPPINGS = [mapping_flatvsall, mapping_collapseflat, mapping_none, mapping_collapseflat_aa, allclasses_aa]


def parse_args():
    parser = argparse.ArgumentParser(description='Generates Confusion Matrices and Metric Reports for final model')
    parser.add_argument('-d','--dataset', default="combined", help='Which test dataset to run against, combined (witten/newywork) or annarbor.', choices=["combined", "annarbor"])
    parser.add_argument('-c','--class_modifier', default="allclasses", help='Whether to collapse classes', choices=["allclasses", "allclasses_aa","collapseflat", "flatvsall", "collapseflat_aa"])
    parser.add_argument('-lg', '--linegraph', action='store_true', help='Output Line Graphs')
    parser.add_argument('-cm', '--confusion_matrix', action='store_true', help='Output Confusion Matrix Graphs')
    parser.add_argument('-t', '--threshold', type=float, default=0.0, help='Default Threshold')

    return parser.parse_args()

def collapse_classes(array, mapping):
    return mapping['integer_mapping'][array]

collapse_classes_vec = np.vectorize(collapse_classes)

def collapse_probs(array, mapping):
    new_arr = np.zeros(shape=(mapping['shape']))
    for key, value in mapping['integer_mapping'].items():
        old_prob = array[key]
        new_arr[value] += old_prob
    # print(array,new_arr)
    return new_arr

def predict_threshold(array, thresh=0.0):
    idx = np.argmax(array)
    prob = array[idx]
    if prob > thresh:
        return idx
    else:
        return 0


# recall, precision, quality, threshold, class

def create_records(metrics, mapping, thresh):
    records = []
    for i in range(metrics.shape[0]):
        row = metrics[i, :]
        records.append({'recall': row[0], 'precision': row[1], 'quality': row[2], 'class': mapping['labels'][i], 'threshold': thresh})
    return records

def threshold_loop(yd_test, yd_pred_probs, mapping, start=0.0, end=.90, steps=10):
    # metrics_all = np.zeros(shape=(10, yd_pred_probs.shape[1] ,3))
    metric_records = []
    # print(metrics_all.shape)
    # Thresholding for predictions
    for i, thresh in enumerate(np.linspace(start, end, steps)):
        yd_pred = np.apply_along_axis(predict_threshold, 1, yd_pred_probs, thresh)
        accuracy = accuracy_score(yd_test, yd_pred)
        cf = confusion_matrix(yd_test, yd_pred)
        metrics = calculate_metrics(cf)
        # metrics_all[i, :,:] = metrics
        # print("Thresh: {}; Accuracy: {}".format(thresh, accuracy))
        # print("Metrics (Recall, Precions, Quality) \n{}".format(metrics))
        metric_records.extend(create_records(metrics, mapping, thresh))

    return metric_records


def get_cm(yd_test, yd_pred_probs, thresh, mapping):
    # Thresholding for predictions
    labels = list(mapping['labels'].values())
    print(labels)
    yd_pred = np.apply_along_axis(predict_threshold, 1, yd_pred_probs, thresh)
    accuracy = accuracy_score(yd_test, yd_pred)
    wrong_predictions = np.nonzero(yd_test - yd_pred)

    print(set(yd_test), set(yd_pred))
    cm = confusion_matrix(yd_test, yd_pred)
    metrics = calculate_metrics(cm)
    print("Accuracy: {}".format(accuracy))
    print("Confusion Matrix: \n{}".format(cm))
    print("Metrics (Recall, Precision, Quality) \n{}".format(metrics))
    return cm, accuracy, wrong_predictions, yd_pred[wrong_predictions]

def get_prediction_probs(mapping, dataset='combined'):
    # Will hold the classes after analyzing train_dir, default class names overided
    classes = ["complex", "complex_flat", "flat", "gabled", "half-hipped", "hipped", "pyramidal", "skillion"]
    # Get all the classifiers
    all_results= joblib.load('results/saved_classifiers.pkl')
    dual_results = [result for result in all_results if result['input'] == 'dual'] 
    # Get the best dual classifier
    best_dual_classifier = max(dual_results, key=lambda x:x['test_acc'])

    # Get Test Data
    test_data_lidar = "trained/encoded/test_resnet50_lidar_{}.pkl".format(dataset)
    test_data_rgb = "trained/encoded/test_inceptionresnetv2_rgb_{}.pkl".format(dataset)
    xl_test, fl_test, yl_test = load_data(test_data_lidar, shuffle=False)
    xr_test, fr_test, yr_test = load_data(test_data_rgb, shuffle=False)
    xd_test = np.concatenate((xl_test, xr_test), axis=1)
    yd_test = yl_test
    # Run best dual classifier through test data set
    yd_pred_probs = best_dual_classifier['classifier'].predict_proba(xd_test)
    # import pdb; pdb.set_trace()
    # Collapse Classes using mapping
    if mapping:
        yd_test = collapse_classes_vec(yl_test, mapping)
        yd_pred_probs = np.apply_along_axis(collapse_probs, 1, yd_pred_probs, mapping)

    return yd_test, yd_pred_probs, fr_test


def create_graph(df, mapping_name, name="metrics_threshold", ):
    with sns.axes_style("darkgrid"):
        df = df[df['class'] != 'unknown']
        current_palette = sns.color_palette()
        # print(current_palette[1])
        def line_plots(x, y1, y2, **kwargs):
            del kwargs['color']
            ax = plt.gca()
            ax.set_ylim(.6, 1.01)
            df = kwargs.pop("data")
            df.plot(x=x, y=y1, kind='line', label='Precision', legend=False, ax=ax, color=current_palette[0] ,**kwargs)
            # ax2 = ax.twinx()
            df.plot(x=x, y=y2, kind='line', ax=ax, label='Recall', legend=False, color=current_palette[1], **kwargs)
            # ax2.set_ylim(.60, 1.01)
            # ax2.yaxis.label.set_visible(False)
            # ax2.set_yticklabels([])
            # import pdb;pdb.set_trace()

        # g = sns.FacetGrid(df_, col="class", col_wrap=3, size=3.5, hue_kws={'color': 'm'}, legend_out=True)
        g = sns.FacetGrid(df, col="class", col_wrap=3)
        g = g.map_dataframe(line_plots, "threshold", "precision", "recall")
        g.set_axis_labels("Confidence Threshold", "Accuracy")
        ax = plt.gca()
        if mapping_name == 'collapseflat':
            lgd = ax.legend(bbox_to_anchor=(-1.70,.25))
        if mapping_name == 'collapseflat_aa':
            lgd = ax.legend(bbox_to_anchor=(-1.70,.25))

        plt.savefig('results/{}_{}.pdf'.format(name, mapping_name), bbox_extra_artists=(lgd,), bbox_inches='tight')

    
def output_wrong_predictions(wrong_predictions_filenames, wrong_predictions, mapping):
    print("{} Wrong Predictions".format(wrong_predictions.shape[0]))
    for i in range(wrong_predictions.shape[0]):
        wrong_predicted_class = mapping['labels'][wrong_predictions[i]]
        # if 'skillion' == wrong_predicted_class:
        #     # import pdb; pdb.set_trace()
        print("File Name: {}; Prediction: {}".format(wrong_predictions_filenames[i], wrong_predicted_class))

def main():
    args = parse_args()
    mapping_list = [mapping for mapping in MAPPINGS if mapping['name'] == args.class_modifier]
    mapping = mapping_list[0]

    yd_test, yd_pred_probs, fl_test = get_prediction_probs(mapping, dataset=args.dataset)
    # import pdb; pdb.set_trace()
    # print(mapping)
    if args.linegraph:
        df = pd.DataFrame(threshold_loop(yd_test, yd_pred_probs, mapping))
        if mapping.get('linegraph'):
            df = df[df['class'].isin(mapping['linegraph']['classes'])]
        df.to_csv('results/metrics_{}.csv'.format(mapping['name']))
        print("Creating Line Graph")
        create_graph(df, mapping['name'])
    
    if args.confusion_matrix:
        print("Plotting Confusion Matrix")
        cm, accuracy, wrong_predictions_mask, wrong_predictions = get_cm(yd_test, yd_pred_probs, args.threshold, mapping)
        output_wrong_predictions(fl_test[wrong_predictions_mask], wrong_predictions, mapping)
        classes = mapping['labels'].values()
        # print(classes)
        plot_confusion_matrix(cm, classes, accuracy, output=mapping['name'], dataset=args.dataset)

if __name__ == '__main__':
    main()



