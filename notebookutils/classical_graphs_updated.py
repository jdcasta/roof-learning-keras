import os
import sys
sys.path.insert(0, os.getcwd())

from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix, classification_report
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

sns.set(font_scale=1.2)  # crazy big


def make_swarm_plot(df, name='test_data_set_classical.pdf'):
    with sns.axes_style("darkgrid"):
        current_palette = sns.color_palette()
        custom_palette = {"InceptionResnetV2": current_palette[1], "Resnet50": current_palette[2], "SVM": current_palette[3], "Random Forest": current_palette[4]}

        ax = sns.swarmplot(x="input", y="test_acc", order=['rgb', 'lidar', 'dual'],hue="name", data=df, palette=custom_palette)
        # fig = plt.gcf()
        # fig.legend(loc=7)
        # ax.legend_.set_title("Models")
        ax.legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
        ax.set_ylim(.45, .90)
        # ax._legend.set_bbox_to_anchor((.76, .35))
        ax.set(xlabel='Input', ylabel='Test Set Accuracy', title='')
        plt.savefig('results/{}'.format(name), bbox_inches="tight")


df = pd.read_csv('./results/classical_training_results.csv')
make_swarm_plot(df)

