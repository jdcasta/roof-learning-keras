# This reads in JSON file and performs the training
from os.path import join as join_path
import argparse
import json
import time
import datetime
import timeit
from pprint import pprint as pp
import itertools as it

import config
from train import set_up_model
from keras import backend as K
from sklearn.externals import joblib
from util import read_test_file, dictproduct


# overwrite the trained directory
TRAINED_DIR = 'multi'
config.trained_dir = join_path(config.trained_dir, TRAINED_DIR)
# print(config.trained_dir)
config.update_paths()
# print(config.fine_tuned_weights_path)

TIME_STAMP = datetime.datetime.fromtimestamp(
    time.time()).strftime('%Y-%m-%d_%H-%M-%S')


def unique_name(test):
    base = test.get('name', test['model'])
    name = '{}_dataset_{}_lr_{}_fc1_{:d}_fc2_{:d}_layer_{}_run_{}_{}_classmodifier_{}'.format(
        base, test['dataset'], test['lr'], test['fc1'], test['fc2'], test['freeze_layers_number'], test['run'], test['data_sub_dir'], test['class_modifier'])
    return name


def create_tests(test_sets):
    test_sets_full = []
    # Iterate through each model test set
    for test_set in test_sets:
        new_test = dict(test_set)  # make a copy of it

        hyperparameters = new_test['hyperparameters']
        hyperparameter_list = dictproduct(hyperparameters)
        for hyper_set in hyperparameter_list:
            full_test = dict(new_test)
            full_test.update(hyper_set)      # add the hyperparameters
            del full_test['hyperparameters']  # dont need this anymore
            # We now have a fully specified test, but we may want to run it more than once
            # to see how much results vary.  We specify this through the repeat key on the test
            # and lable the index of the repeat with the key 'run'
            for run in range(full_test['repeat']):
                full_test_repeat = dict(full_test)  # make a copy
                full_test_repeat['run'] = run
                del full_test_repeat['repeat']      # dont need this anymore
                # full_test_repeat['full_name'] = unique_name(full_test_repeat)
                # add to the full test list
                test_sets_full.append(full_test_repeat)

    return test_sets_full


def run_test(test):
    history_dict = {}
    # Specify the full name
    test['full_name'] = unique_name(test)
    start_time = timeit.default_timer()
    try:
        model = set_up_model(**test)
        history = model.train(save_full_model=False)

        elapsed = timeit.default_timer() - start_time
        results = {'time': elapsed, 'params': test}
        results.update(history.history)
        history_dict[test['full_name']] = results
    except (KeyboardInterrupt, SystemExit):
        print('Cancelling training of model early')
    finally:
        K.clear_session()
        del model
    return history_dict


def run_tests(test_sets, name='UK'):
    " Runs our test suite "
    history_dict = {}
    for test in test_sets:
        # NOTE: We can't handle frozen_layers as a hyperparameter
        # An important not is that we freeze layers during the training process
        # We first freeze the first X number of layers and only change the weights on the top layers
        # After training (probably getting not very good results), we then train the same model witht he SAME
        # Hyperparameters again, but by unfreezing a few more layers.
        if not test.get('active', True):
            print('Skipping test...')
            continue
        history_dict_individual = run_test(test)
        history_dict.update(history_dict_individual)
        joblib.dump(history_dict, join_path(config.trained_dir,
                                            "{}_history_{}.pkl".format(name, TIME_STAMP)))

    joblib.dump(history_dict, join_path(config.trained_dir,
                                        "{}_history_{}.pkl".format(name, TIME_STAMP)))
    print('end')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', required=True,
                        help='Path to input test file')
    args = parser.parse_args()
    # An array of information for each test set
    test_sets, name = read_test_file(args.input)
    pp(test_sets)

    test_sets_full = create_tests(test_sets)
    pp(test_sets_full)
    run_tests(test_sets_full, name=name)


if __name__ == '__main__':
    main()
