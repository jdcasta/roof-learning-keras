from os.path import join as join_path
import numpy as np
import argparse
import traceback
import os
import keras

import pandas as pd


np.random.seed(1337)  # for reproducibility
# keras.backend.set_image_data_format('channels_last')

import util
import config
# Disable tensorflow warnings about not compling library for advanced CPU optimizations
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', help='Path to data dir')
    parser.add_argument('--csv', help='Path to csv file', required=True)
    parser.add_argument('--classes', nargs='+', help='Classes to include', required=False, default=[])
    parser.add_argument(
        '--data_sub_dir', help='If there is a specific sub directory to train from', default='dual')

    parser.add_argument('--model', type=str, required=True, help='Base model architecture',
                        choices=[config.MODEL_RESNET50, config.MODEL_RESNET152, config.MODEL_INCEPTION_V3,
                                 config.MODEL_VGG16, config.MODEL_VGG16_DUAL, config.MODEL_RESNET50DUAL, config.MODEL_INCEPTION_RESNET_V2])
    parser.add_argument('--continue_tr', action='store_true',
                        help='To continue training where left off')
    parser.add_argument('--weights', nargs='+', help='path to weights file', default=None, type=str, required=False)
    parser.add_argument('--dual_input', action="store_true", help='If we are training on two input images',
                        default=False)
    parser.add_argument('--nb_epoch', type=int, default=1000)
    parser.add_argument('--lr', type=float, default=config.LR)
    parser.add_argument('--nb_batch_size', type=int, default=16)
    parser.add_argument('--freeze_layers_number', type=int,
                        help='will freeze the first N layers and unfreeze the rest')
    return parser.parse_args()

def set_up_model(data_dir=None, csv=None, classes=[], model=config.model, data_sub_dir=config.data_sub_dir, full_name=None,
          dual_input=config.dual_input, nb_epoch=1000, nb_batch_size=config.nb_batch_size, weights=None, freeze_layers_number=None, lr=config.LR, continue_tr=False, initial_epoch=0,
          fc1=config.FC1_SIZE, fc2=config.FC2_SIZE, pooling=config.POOLING, class_modifier='none', dataset=config.dataset, **kwargs):
    
    if csv is None:
        raise ValueError("csv must point to a valid path")
    if not os.path.isfile(csv):
        csv = os.path.join(data_dir, csv)
    config.df = pd.read_csv(csv)
    


    if not full_name:
        full_name = model

    print(locals())

    # Hyperparameters
    config.LR = lr
    config.FC1_SIZE = fc1
    config.FC2_SIZE = fc2
    config.POOLING = pooling

    config.continue_training = continue_tr
    config.initial_epoch = initial_epoch
    config.nb_batch_size = nb_batch_size
    config.full_name = full_name
    config.class_modifier = class_modifier

    config.model = model
    config.dataset = dataset

    # This is where I need to rethink
    config.data_dir = data_dir
    # config.set_paths(dual_input=dual_input,
    #                     sub_dir=data_sub_dir)
    config.dual_input = dual_input
    config.data_sub_dir = data_sub_dir
    if not classes:
        print('Getting classes from dataframe, risky...')
        util.set_classes_from_train_dir()
    else:
        config.classes = classes
    # determines number of images to train on
    util.set_samples_info(config.class_modifier)
    if not os.path.exists(config.trained_dir):
        os.mkdir(config.trained_dir)

    class_weight = util.get_class_weight(config.df_train)

    model = util.get_model_class_instance(
        class_weight=class_weight,
        nb_epoch=nb_epoch,
        freeze_layers_number=freeze_layers_number,
        pooling=config.POOLING)

    if config.continue_training or config.dual_input:
        model.load_weights(weights=weights)

    return model



def train(model, **kwargs):

    print('Starting training of {}'.format(config.full_name))
    try:
        util.lock()
        history = model.train()
        print('Training is finished!')
        return history
    except (KeyboardInterrupt, SystemExit):
        print(traceback.format_exc())
        util.unlock()
    except Exception as e:
        print(e)
        print(traceback.format_exc())
    util.unlock()


if __name__ == '__main__':
    print('Starting...')
    args = parse_args()
    args = vars(args)
    model1 = set_up_model(**args)
    train(model1)
