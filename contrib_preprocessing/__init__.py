import os
import sys
import keras
from keras_preprocessing.image import Iterator, ImageDataGenerator, load_img, get_keras_submodule, img_to_array, array_to_img
import pandas as pd
import numpy as np

# from keras_preprocessing import get_keras_submodule
backend = get_keras_submodule('backend')
class DataframeIterator(Iterator):
    """Iterator capable of reading images from a dataframe.
    # Arguments
        dataframe: Dataframe containing file name and sources.
        image_data_generator: Instance of `ImageDataGenerator`
            to use for random transformations and normalization.
        directory: Path to the directory to read images from.
            Defaults to and empty string, assuming dataframe has full file path
            Will be used with join
        class_column: String, the column name containing the class
        src_column: String, the column name containing the src for the picture
        target_size: tuple of integers, dimensions to resize input images to.
        color_mode: One of `"rgb"`, `"rgba"`, `"grayscale"`.
            Color mode to read images.
        classes: Optional list of strings, names of subdirectories
            containing images from each class (e.g. `["dogs", "cats"]`).
            It will be computed automatically if not set.
        class_mode: Mode for yielding the targets:
            `"binary"`: binary targets (if there are only two classes),
            `"categorical"`: categorical targets,
            `"sparse"`: integer targets,
            `"input"`: targets are images identical to input images (mainly
                used to work with autoencoders),
            `None`: no targets get yielded (only input images are yielded).
        batch_size: Integer, size of a batch.
        shuffle: Boolean, whether to shuffle the data between epochs.
        seed: Random seed for data shuffling.
        data_format: String, one of `channels_first`, `channels_last`.
        save_to_dir: Optional directory where to save the pictures
            being yielded, in a viewable format. This is useful
            for visualizing the random transformations being
            applied, for debugging purposes.
        save_prefix: String prefix to use for saving sample
            images (if `save_to_dir` is set).
        save_format: Format to use for saving sample images
            (if `save_to_dir` is set).
        subset: Subset of data (`"training"` or `"validation"`) if
            validation_split is set in ImageDataGenerator.
        interpolation: Interpolation method used to resample the image if the
            target size is different from that of the loaded image.
            Supported methods are "nearest", "bilinear", and "bicubic".
            If PIL version 1.1.3 or newer is installed, "lanczos" is also
            supported. If PIL version 3.4.0 or newer is installed, "box" and
            "hamming" are also supported. By default, "nearest" is used.
    """

    def __init__(self, dataframe, image_data_generator,
                 directory='', 
                 class_column='class', src_column='src',
                 target_size=(256, 256), color_mode='rgb',
                 classes=None, class_mode='categorical',
                 batch_size=32, shuffle=True, seed=None,
                 data_format=None,
                 save_to_dir=None, save_prefix='', save_format='png',
                 subset=None,
                 interpolation='nearest'):
        if data_format is None:
            data_format = backend.image_data_format()
        self.directory = directory
        self.dataframe = dataframe
        self.image_data_generator = image_data_generator
        self.target_size = tuple(target_size)
        if color_mode not in {'rgb', 'rgba', 'grayscale'}:
            raise ValueError('Invalid color mode:', color_mode,
                             '; expected "rgb", "rgba", or "grayscale".')
        self.color_mode = color_mode
        self.data_format = data_format
        if self.color_mode == 'rgba':
            if self.data_format == 'channels_last':
                self.image_shape = self.target_size + (4,)
            else:
                self.image_shape = (4,) + self.target_size
        elif self.color_mode == 'rgb':
            if self.data_format == 'channels_last':
                self.image_shape = self.target_size + (3,)
            else:
                self.image_shape = (3,) + self.target_size
        else:
            if self.data_format == 'channels_last':
                self.image_shape = self.target_size + (1,)
            else:
                self.image_shape = (1,) + self.target_size
        self.classes = classes
        if class_mode not in {'categorical', 'binary', 'sparse',
                              'input', None}:
            raise ValueError('Invalid class_mode:', class_mode,
                             '; expected one of "categorical", '
                             '"binary", "sparse", "input"'
                             ' or None.')
        self.class_mode = class_mode
        self.save_to_dir = save_to_dir
        self.save_prefix = save_prefix
        self.save_format = save_format
        self.interpolation = interpolation

        if subset is not None:
            validation_split = self.image_data_generator._validation_split
            if subset == 'validation':
                split = (0, validation_split)
            elif subset == 'training':
                split = (validation_split, 1)
            else:
                raise ValueError(
                    'Invalid subset name: %s;'
                    'expected "training" or "validation"' % (subset,))
        else:
            split = None
        self.subset = subset

        # number of samples is simply the row count of the dataframe
        self.samples = self.dataframe.shape[0]
        if not classes:
            print('Classes not provided!!!')
            sys.exit()
            classes = sorted(list(self.dataframe[class_column].unique()))

        self.num_classes = len(classes)
        self.class_indices = dict(zip(classes, range(len(classes))))
        # print(self.class_indices)

        print('Found %d images belonging to %d classes.' %
              (self.samples, self.num_classes))

        # Extract file names and class indexes from dataframe
        self.filenames = dataframe[src_column].values
        self.classes = self.dataframe[class_column].apply(lambda class_: self.class_indices[class_]).values
        # print(self.classes)


        super(DataframeIterator, self).__init__(self.samples,
                                                batch_size,
                                                shuffle,
                                                seed)

    def _get_batches_of_transformed_samples(self, index_array):
        batch_x = np.zeros(
            (len(index_array),) + self.image_shape,
            dtype=backend.floatx())
        # build batch of image data
        for i, j in enumerate(index_array):
            fname = self.filenames[j]
            img = load_img(os.path.join(self.directory, fname),
                           color_mode=self.color_mode,
                           target_size=self.target_size,
                           interpolation=self.interpolation)
            x = img_to_array(img, data_format=self.data_format)
            # Pillow images should be closed after `load_img`,
            # but not PIL images.
            if hasattr(img, 'close'):
                img.close()
            params = self.image_data_generator.get_random_transform(x.shape)
            x = self.image_data_generator.apply_transform(x, params)
            x = self.image_data_generator.standardize(x)
            batch_x[i] = x
        # optionally save augmented images to disk for debugging purposes
        if self.save_to_dir:
            for i, j in enumerate(index_array):
                img = array_to_img(batch_x[i], self.data_format, scale=True)
                fname = '{prefix}_{index}_{hash}.{format}'.format(
                    prefix=self.save_prefix,
                    index=j,
                    hash=np.random.randint(1e7),
                    format=self.save_format)
                img.save(os.path.join(self.save_to_dir, fname))
        # build batch of labels
        if self.class_mode == 'input':
            batch_y = batch_x.copy()
        elif self.class_mode == 'sparse':
            batch_y = self.classes[index_array]
        elif self.class_mode == 'binary':
            batch_y = self.classes[index_array].astype(backend.floatx())
        elif self.class_mode == 'categorical':
            batch_y = np.zeros(
                (len(batch_x), self.num_classes),
                dtype=backend.floatx())
            for i, label in enumerate(self.classes[index_array]):
                batch_y[i, label] = 1.
        else:
            return batch_x
        return batch_x, batch_y

    def next(self):
        """For python 2.x.
        # Returns
            The next batch.
        """
        with self.lock:
            index_array = next(self.index_generator)
        # The transformation of images is not under thread lock
        # so it can be done in parallel
        return self._get_batches_of_transformed_samples(index_array)

    
def flow_from_dataframe(self, dataframe,
                        directory='', 
                        class_column='class',
                        src_column='src',
                        target_size=(256, 256), color_mode='rgb',
                        classes=None, class_mode='categorical',
                        batch_size=32, shuffle=True, seed=None,
                        save_to_dir=None,
                        save_prefix='',
                        save_format='png',
                        subset=None,
                        interpolation='nearest'):
    """Takes the path to a directory & generates batches of augmented data.
    # Arguments
        directory: Path to the directory to read images from.
            Defaults to and empty string, assuming dataframe has full file path
            Will be used with join
        class_column: String, the column name containing the class
        src_column: String, the column name containing the src for the picture
        target_size: Tuple of integers `(height, width)`,
            default: `(256, 256)`.
            The dimensions to which all images found will be resized.
        color_mode: One of "grayscale", "rbg", "rgba". Default: "rgb".
            Whether the images will be converted to
            have 1, 3, or 4 channels.
        classes: Optional list of class subdirectories
            (e.g. `['dogs', 'cats']`). Default: None.
            If not provided, the list of classes will be automatically
            inferred from the subdirectory names/structure
            under `directory`, where each subdirectory will
            be treated as a different class
            (and the order of the classes, which will map to the label
            indices, will be alphanumeric).
            The dictionary containing the mapping from class names to class
            indices can be obtained via the attribute `class_indices`.
        class_mode: One of "categorical", "binary", "sparse",
            "input", or None. Default: "categorical".
            Determines the type of label arrays that are returned:
            - "categorical" will be 2D one-hot encoded labels,
            - "binary" will be 1D binary labels,
                "sparse" will be 1D integer labels,
            - "input" will be images identical
                to input images (mainly used to work with autoencoders).
            - If None, no labels are returned
              (the generator will only yield batches of image data,
              which is useful to use with `model.predict_generator()`,
              `model.evaluate_generator()`, etc.).
              Please note that in case of class_mode None,
              the data still needs to reside in a subdirectory
              of `directory` for it to work correctly.
        batch_size: Size of the batches of data (default: 32).
        shuffle: Whether to shuffle the data (default: True)
        seed: Optional random seed for shuffling and transformations.
        save_to_dir: None or str (default: None).
            This allows you to optionally specify
            a directory to which to save
            the augmented pictures being generated
            (useful for visualizing what you are doing).
        save_prefix: Str. Prefix to use for filenames of saved pictures
            (only relevant if `save_to_dir` is set).
        save_format: One of "png", "jpeg"
            (only relevant if `save_to_dir` is set). Default: "png".
        subset: Subset of data (`"training"` or `"validation"`) if
            `validation_split` is set in `ImageDataGenerator`.
        interpolation: Interpolation method used to
            resample the image if the
            target size is different from that of the loaded image.
            Supported methods are `"nearest"`, `"bilinear"`,
            and `"bicubic"`.
            If PIL version 1.1.3 or newer is installed, `"lanczos"` is also
            supported. If PIL version 3.4.0 or newer is installed,
            `"box"` and `"hamming"` are also supported.
            By default, `"nearest"` is used.
    # Returns
        A `DirectoryIterator` yielding tuples of `(x, y)`
            where `x` is a numpy array containing a batch
            of images with shape `(batch_size, *target_size, channels)`
            and `y` is a numpy array of corresponding labels.
    """
    return DataframeIterator(
        dataframe, self, directory=directory,
        class_column=class_column, src_column=src_column,
        target_size=target_size, color_mode=color_mode,
        classes=classes, class_mode=class_mode,
        data_format=self.data_format,
        batch_size=batch_size, shuffle=shuffle, seed=seed,
        save_to_dir=save_to_dir,
        save_prefix=save_prefix,
        save_format=save_format,
        subset=subset,
        interpolation=interpolation)
    

def monkey_patch_all():
    ImageDataGenerator.flow_from_dataframe = flow_from_dataframe