import tensorflow as tf
import keras
print("Version {}".format(tf.__version__))

hello = tf.constant('Hello, TensorFlow!')
sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
print(sess.run(hello))

print(keras.__version__)