"""
This script will go through a directory and split the data into train, valid, and test directories.
You specify a split with command line parameters. This is splitting FILES, not in memory data.
This is a COPY split, not a MOVE
"""
import os
import shutil
from os import path
import argparse
import random

SUBDIRS = ['rgb', 'lidar']
SPLIT_DIRS = ['train', 'valid', 'test']

def make_dirs(root_dirs, second_layers, third_layers):
    """ Creates class subdirectories inside the given directory"""
    if isinstance(root_dirs, str):
        root_dirs = [root_dirs]
    else:
        root_dirs = root_dirs
    for root_dir in root_dirs:
        for second_layer in second_layers:
            for third_layer in third_layers:
                new_class_dir = os.path.join(root_dir, second_layer, third_layer)
                if not os.path.exists(new_class_dir):
                    os.makedirs(new_class_dir)

def perform_split(category_dir, ratio, dest_dir):
    " Given a cateogry dir, move files out of and into train, valid, and test directories "
    all_files = os.listdir(category_dir)        # list of all files
    random.shuffle(all_files)                   # shuffle the files
    num_files = len(all_files)                  # number of files
    category_name = path.basename(category_dir) # category dir name

    # This will hold a list of how many files to copy for train,valid,test respectively
    ratio_num = [int(rat/100 * num_files) for rat in ratio]

    # Iterate through each split directory and the amount to copy
    for i, (split_dir, num) in enumerate(zip(SPLIT_DIRS, ratio_num)):
        # If we are on the last split, just copy all the leftover files
        if i == 2:
            num = len(all_files)
        # Pop a file from the list and move it
        for _ in range(num):
            file_name_rgb = all_files.pop()
            file_name_lidar = file_name_rgb.replace('rgb', 'lidar').replace('.jpg', '.png')
            src_path_rgb = path.join(category_dir, file_name_rgb)
            src_path_lidar = src_path_rgb.replace('rgb', 'lidar').replace('.jpg', '.png')
            dest_path_rgb = path.join(dest_dir, split_dir, 'rgb', category_name, file_name_rgb)
            dest_path_lidar = path.join(dest_dir, split_dir, 'lidar', category_name, file_name_lidar)

            shutil.copy(src_path_rgb, dest_path_rgb)
            shutil.copy(src_path_lidar, dest_path_lidar)


def main():
    " Main Function "
    (data_dir, ratio, dest_dir) = parse_args()
    # get all categories
    categories = os.listdir(path.join(data_dir, SUBDIRS[0]))
    # create all the new directories in the destination directory
    make_dirs([path.join(dest_dir, split_dir) for split_dir in SPLIT_DIRS], SUBDIRS, categories)
    print('Beggining to copy files...')
    for category in categories:
        cateogry_dir = path.join(data_dir, SUBDIRS[0], category)
        perform_split(cateogry_dir, ratio, dest_dir)

    print('Finished Copying All Files')


def parse_args():
    " Parses Args "
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', help='Path to data dir')
    parser.add_argument('--split', help='The split ration of train/valid/test. Eg. --split 60,30,10', default="60,30,10")
    parser.add_argument('--dest_dir', help='Path to dest dir', default='imgs')

    args = parser.parse_args()
    split_ratio = list(map(int, args.split.split(',')))
    return (args.data_dir, split_ratio, args.dest_dir)

if __name__ == '__main__':
    main()
