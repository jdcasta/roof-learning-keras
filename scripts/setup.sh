#!/bin/bash
module add python-anaconda3
module load cuda/8.0.44
module load cudnn/8.0-v5.1

# git clone https://jdcasta@bitbucket.org/jdcasta/roof-learning-keras.git
# python scripts/download-data.py 0B8g4dTIwwj_mNm14anJaUGNmNkk imgs/pics.zip
# cd imgs
# unzip pics.zip
# cd ..