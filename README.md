# Deep Learning for Roof Shapes

Much of this code has been taken and modified from [here](https://github.com/Arsey/keras-transfer-learning-for-oxford102). However it doesnt look to much like it anymore.

# Tensorflow Dependencies (Binaries and Libraries for GPU computing)

Please install the tensorflow dependencies (Tensor flow 1.2). You can find instructions [here](https://www.tensorflow.org/versions/r1.2/install/install_linux).

Just to name the primary ones:

* CUDA 9.0
* NVIDIA Driver for GPU
* CUDNN
* libcupti-dev 

## Python Set Up

Please ensure that you have anaconda (ver3) or are using virtual env and set up a new python virtual envrionment. Read [here](http://docs.python-guide.org/en/latest/dev/virtualenvs/) for details.

Install with:
```pip install -r requirements.txt```


python train.py --data_dir D:/Research/roof_learning/witten/pics --data_sub_dir rgb --model vgg16


## Confidence Thresholds

Note on 11/14/2018 - I had a freakout thinking that my graphs in the confidence thresholds dont match what I reported for precision and recall. I assumed that the recall and precision with a confidence threshold of 0, should **exactly** match the precsion and recall already reported.  However that is NOT the case! I combined the flat and complex-flat classes for these graphs.  This automatically changes the probability distribution, affecting the discrete choice of the model.  In other words before:

Flat - .3, CF - .3, Skillion - .4 - FINAL choice Skillion
New model
Flat-like - .6, Skillion .4 - FInal choice flatlike

The model changes even without a change in the confidence threshold.



