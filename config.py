from os.path import join as join_path
import os

import keras
# THe absolute path of this file
abspath = os.path.dirname(os.path.abspath(__file__))
# Lock file to prevent process
lock_file = os.path.join(abspath, 'lock')

# Default directory for data
data_dir = join_path(abspath, 'data/')
# Directory to store trained models
trained_dir = join_path(abspath, 'trained')
models_dir = join_path(trained_dir, 'models')


keras_version = keras.__version__.split('.')

# Will contain paths to training directory and validation dir
train_dirs, validation_dirs, test_dirs = None, None, None

# If training on two sets of images (multiple modalities)
trunk_1 = 'rgb'
trunk_2 = 'lidar'
dual_input = False
data_sub_dir = 'dual'
full_name = None # The full specified name to be used when saving the model, only used in multi-train
class_modifier = 'none' # modifies class names after loading csv file

# AVAILABLE MODELS
MODEL_VGG16 = 'vgg16'
MODEL_VGG16_DUAL = 'vgg16_dual'
MODEL_VGG19 = 'vgg19'
MODEL_INCEPTION_V3 = 'inceptionv3'
MODEL_RESNET50 = 'resnet50'
MODEL_INCEPTION_RESNET_V2 = 'inceptionresnetv2'
MODEL_RESNET50DUAL = 'resnet50dual'
MODEL_RESNET152 = 'resnet152'

# AVAILABLE HYPER PARAMETER FOR TUNING, DEFAULT VALUES
LR = 1.0
FC1_SIZE = 100
FC2_SIZE = 100
PATIENCE = 7
POOLING= 'avg'

# DEFAULT MODEL
model = MODEL_VGG16

fine_tuned_weights_path = join_path(trained_dir, 'fine-tuned-{}-{}-weights.h5')
model_path = join_path(trained_dir, 'model-{}-{}.h5')
classes_path = join_path(trained_dir, 'classes')

# DIRECTORIES TO SAVE MODELS AND WEIGHTS
def update_paths():
    global fine_tuned_weights_path, model_path, classes_path
    fine_tuned_weights_path = join_path(trained_dir, 'fine-tuned-{}-{}-weights.h5')
    model_path = join_path(trained_dir, 'model-{}-{}.h5')
    classes_path = join_path(trained_dir, 'classes')


# Will hold the classes after analyzing train_dir, default class names overided
classes = ["complex", "complex_flat", "flat", "gabled", "half-hipped", "hipped", "pyramidal", "skillion"]

# Batch Size for fit_generator and total number of samples
nb_batch_size = 16

# Number of samples for train, validation, and test directories
# This is filled automatically after calling util.set_samples()
nb_train_samples = 0
nb_validation_samples = 0
nb_test_samples = 0

# How many times to cycle through all images until an epoch is considered complete
# Useful if using data augmentation and you want to go through all the augmented
# possibilities, we have a total of 8 augmentated possibilities
picture_cycles = 2

# If continuing training (load previously saved model)
continue_training = False
initial_epoch = 0


## Dataframe of files
df = None
df_train = None
df_val = None
df_test = None

# Dataset, what dataset are we training on
dataset = 'witten'


# Initialize the paths
# set_paths()

# Functions which configure paths for saved models

def get_fine_tuned_weights_path(trunk=None):
    "Gets the fine tuned weights path"
    if not trunk:
        trunk = data_sub_dir
    # End of removal
    return fine_tuned_weights_path.format(full_name, trunk)


def get_model_path(trunk=None):
    "Gets the model path"
    if not trunk:
        trunk = data_sub_dir
    return model_path.format(full_name, trunk)

# I dont really need individual classes types, so just use model string
def get_classes_path():
    " Gets the classes path for loading "
    return classes_path.format(model)
