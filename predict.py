" Performs predictions on a specific subdirectory of pictures "
import time
import sys
import argparse
from os.path import join as join_path
import json
import math
from pprint import pprint as pp
import numpy as np
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.externals import joblib
import keras
from keras import backend as K
from colorama import Fore, Style
from train import set_up_model

# keras.backend.set_image_data_format('channels_last')

np.random.seed(1337)  # for reproducibility

import config
import util


PRED_THRESHOLD = 50

def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-i',
        '--input',  help='Input File for Multi Predictions', default=None, type=str)
    parser.add_argument(
        '--data_dir',  help='Path to data directory', default=None, type=str)
    parser.add_argument(
        '--data_sub_dir', help='If there is a specific sub directory to train from')
    parser.add_argument('--dual_input', action='store_true',
                        help='If we are training on two input images')
    parser.add_argument('--accuracy', action='store_true',
                        help='To print accuracy score')
    parser.add_argument('--fc1', default=config.FC1_SIZE, type=int,
                        help='Size of Connected FC1 layer')
    parser.add_argument('--verbose', action='store_true',
                        help='Verbose Logging')                            
    parser.add_argument('--weights', nargs='+', help='path to weights file', default=None, type=str, required=False)
    parser.add_argument('--plot_confusion_matrix', action='store_true')
    parser.add_argument('--model', type=str, help='Base model architecture',
                        choices=[config.MODEL_RESNET50, config.MODEL_RESNET152, config.MODEL_INCEPTION_V3, config.MODEL_VGG16, config.MODEL_VGG16_DUAL, config.MODEL_INCEPTION_RESNET_V2])
    parser.add_argument('--batch_size', default=48, type=int,
                        help='How many files to predict on at once')
    args = parser.parse_args()
    return args

def predict2(model_module, modality='rgb', train_val_test='test', verbose=False, plot_confusion_matrix=True, output=None, dataset='UK', **kwargs):
    
    classes_in_keras_format = util.get_classes_in_keras_format()
    df = util.get_subset_df(config.df, train_val_test)
    test_generator =util.image_generator_df(df, config.data_dir, modality=modality, target_size=model_module.img_size, batch_size=config.nb_batch_size, shuffle=False, augment=False, preprocess_input=model_module.preprocess_input, seed=1)
    num_samples = df.shape[0]

    # The true labels and predicted labels
    y_trues = np.zeros(num_samples)
    predictions = np.zeros(num_samples)
    # File names for debugging purposes
    file_names = []

    all_probs = []
    num_loops = math.ceil(num_samples / config.nb_batch_size)
    for i, (inputs, classes) in enumerate(test_generator):
        # print(len(inputs))
        # print(inputs[0].shape, inputs[1].shape)
        if i >= num_loops:
            break
        # calculate the indexes
        n_from = i * config.nb_batch_size
        n_to = (n_from + config.nb_batch_size) if i < num_loops - \
            1 else num_samples
        # print(n_from, n_to)
        # get the true labels
        y_true = np.argmax(classes, axis=1)
        # print(inputs.shape, classes.shape)
        y_trues[n_from:n_to] = y_true
        # model.predict(inputs)
        # import pdb
        # pdb.set_trace()
        file_name = list(test_generator.original.filenames[n_from: n_to])
        file_names += file_name
        # print(file_name)

        # Make predictions
        start = time.clock()
        # import pdb
        # pdb.set_trace()
        probs = model_module.model.predict(inputs)
        prediction = np.argmax(probs, axis=1)
        end = time.clock()
        predictions[n_from:n_to] = prediction
        if verbose:
            print('Prediction on batch {} took: {}'.format(i, end - start))

        # Just for checking the probabilities
        probs = (probs * 100).astype(np.int32).tolist()
        for prob, file in zip(probs, file_name):
            all_probs.append({'file': file, 'prob': prob, 'above_thresh': max(prob) > PRED_THRESHOLD})
        # print(probs)

    # pp([prob for prob in all_probs if not prob['above_thresh']])
    inv_map = {v: k for k, v in classes_in_keras_format.items()}
    for i, (prediction, y_true) in enumerate(zip(predictions, y_trues)):
        # import pdb
        # pdb.set_trace()
        recognized_class_prediction = inv_map[int(prediction)]
        recognized_class_true = inv_map[int(y_true)]
        file_name = file_names[i]
        color = Fore.GREEN if y_true == prediction else Fore.RED
        if verbose and y_true != prediction:
            print(color + '{} | should be {} ({}) -> predicted as {} ({})'.format(file_name, y_true, recognized_class_true, prediction,
                                                                              recognized_class_prediction))
    print(Style.RESET_ALL)
    accuracy = accuracy_score(y_true=y_trues, y_pred=predictions)
    print('Accuracy {}'.format(accuracy))


    if plot_confusion_matrix:
        cnf_matrix = confusion_matrix(y_trues, predictions)
        util.plot_confusion_matrix(
            cnf_matrix, config.classes, accuracy, normalize=False, sub_dir=modality, output=output, dataset=dataset)
        util.plot_confusion_matrix(
            cnf_matrix, config.classes, accuracy, normalize=True, sub_dir=modality, output=output, dataset=dataset)

    return accuracy


"""Runs a prediction run

Returns:
    number -- Accuracy of the model on the dataset
"""

def run_predict(output=None, verbose=False, plot_confusion_matrix=True, train_val_test='test', **kwargs):
    model = set_up_model(**kwargs)
    # model.save_model()
    # sys.exit()
    accuracy = predict2(model, modality=config.data_sub_dir, train_val_test=train_val_test, verbose=verbose, plot_confusion_matrix=plot_confusion_matrix, output=output, **kwargs)

    K.clear_session()
    del model
    
    return accuracy

def generate_tests(file_name):
    with open(file_name) as data_file:
        data = json.load(data_file)
    full_tests = []
    # pp(data)
    for test_set in data['test_sets']:
        defaults = dict(data['defaults'])
        defaults.update(test_set)
        full_tests.append(defaults)

    return full_tests
    
def main():
    " Main Function in script file "
    args = parse_args()
    print('=' * 50)
    print('Called with args:')
    print(args)
    if args.input:
        tests, name =util.read_test_file(args.input)
        results = []
        for test in tests:
            # print("Running Test:")
            # pp(test)
            accuracy = run_predict(**test)
            results.append({ 'name': test['name'], test['type']: accuracy})
        pp(results)
    else:
        run_predict(**vars(args))


if __name__ == '__main__':
    main()

