"""
This module will predict flat roof shapes
"""


import os
import sys
import argparse
from pprint import pprint as pp
import logging
import json
import sqlite3

logger = logging.getLogger('predict_rf')
logger.setLevel(logging.INFO)

from sklearn.externals import joblib
import numpy as np
import pandas as pd

# import util
np.set_printoptions(precision=3, suppress=True,
                    linewidth=200, threshold=100000)


UPDATE_ROOFSHAPE_ITEM = """
UPDATE {0}
SET {1} = :roof_shape_predicted
WHERE {2} = :uid
"""

mapping_collapseflat = {
    "name": "collapseflat",
    "integer_mapping": {
        0: 0,
        1: 1,
        2: 1,
        3: 2,
        4: 3,
        5: 4,
        6: 5,
        7: 6
    },
    "shape": 7,
    "labels": {
        0: "unknown",
        1: "flat-like",
        2: "gabled",
        3: "half-hipped",
        4: "hipped",
        5: "pyramidal",
        6: "skillion"
    }
}


def parse_args():
    parser = argparse.ArgumentParser(
        description='Generate roof shape predictions and store in database')
    parser.add_argument('-i', '--input', default="combined",
                        help='Input file to configure prediction')

    return parser.parse_args()


def load_data(file_name):
    data_set = joblib.load(file_name)
    features, files, labels = data_set['features'], data_set['file_names'], data_set['labels']
    return features


def collapse_classes(array, mapping):
    return mapping['integer_mapping'][array]


collapse_classes_vec = np.vectorize(collapse_classes)


def collapse_probs(array, mapping):
    new_arr = np.zeros(shape=(mapping['shape']))
    for key, value in mapping['integer_mapping'].items():
        old_prob = array[key]
        new_arr[value] += old_prob
    # print(array,new_arr)
    return new_arr


def predict_threshold(array, thresh=0.0):
    idx = np.argmax(array)
    prob = array[idx]
    if prob >= thresh:
        return idx
    else:
        return 0


def predict_rf(config, mapping=mapping_collapseflat):
    logger.info('Starting RF Predict on %s', config['name'])
    df = pd.read_csv(config['csv_file'], index_col='uid')
    # unique ids of each building
    uids = df.index.tolist()
    # lidar and rgb features
    xl_test = load_data(config['lidar_encoding'])
    xr_test = load_data(config['rgb_encoding'])
    # join features
    xd_test = np.concatenate((xl_test, xr_test), axis=1)

    classifier = joblib.load(config['rf_model'])
    yd_pred_probs = classifier['classifier'].predict_proba(xd_test)

    # Probabilities for each class
    yd_pred_probs = np.apply_along_axis(
        collapse_probs, 1, yd_pred_probs, mapping)
    # Argmax with thresholding, gives class index
    yd_pred = np.apply_along_axis(
        predict_threshold, 1, yd_pred_probs, config['threshold'])
    # converts class index to a label (string)
    yd_pred_classes = list(map(lambda x: mapping['labels'][x], yd_pred))

    db_updates = [{'uid': uid, 'roof_shape_predicted': roof_shape}
                  for (uid, roof_shape) in zip(uids, yd_pred_classes)]
    # pp(db_updates)
    return db_updates


def update_db(db_updates, config):
    conn = sqlite3.connect(config['osm']['db_path'])
    curs = conn.cursor()

    table_name = config['osm']['table_name']
    uid_key = config['osm']['uid']
    roof_shape_key = config['osm']['roof_shape']
    qry = UPDATE_ROOFSHAPE_ITEM.format(table_name, roof_shape_key, uid_key)
    for update in db_updates:
        curs.execute(qry, update)

    conn.commit()


def main():
    """Makes a prediction on roof shape and stores within a database
    """

    args = parse_args()

    with open(args.input) as f:
        data = json.load(f)

    for datum in data:
        db_updates = predict_rf(datum)
        update_db(db_updates, datum)


if __name__ == '__main__':
    main()
